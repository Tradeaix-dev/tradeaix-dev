({
    getHelps : function(component, event, helper) {
        component.set("v.helpdec",true);
        var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
        document.title =sitename +' : Help';
    },
    Submitrequestclick : function(component, event) { 
        debugger;        
        component.set("v.helpdec",false);
        component.set("v.Submitrequest", true);       
    },
    canceladd: function(component, event) { 
        debugger;        
        component.set("v.helpdec",true);
        component.set("v.Submitrequest", false);       
    },
    SubmitReq: function(component, event) { 
        debugger;        
        var errorFlag = 'false';
        var Type = component.find("Type").get("v.value");
        var Priority = component.find("Priority").get("v.value");
        var Category = component.find("Category").get("v.value");
        var Status = component.find("Status").get("v.value");
        var Subject = component.find("Subject").get("v.value");
        var Description = component.find("Description").get("v.value");
        
        if(Type == '' || Type =='--Select--' || Type == 'undefined' || Type == null) {
            errorFlag = 'true';
            component.set("v.TypeError", 'Input field required'); 
        } else { 
            component.set("v.TypeError", ''); 
        } 
        if(Category == '' || Category =='--Select--' || Category == 'undefined' || Category == null) {
            errorFlag = 'true';
            component.set("v.catError", 'Input field required'); 
        } else { 
            component.set("v.catError", ''); 
        } 
        if(Priority == '' || Priority =='--Select--' || Priority == 'undefined' || Priority == null) {
            errorFlag = 'true';
            component.set("v.PriorityError", 'Input field required'); 
        } else { 
            component.set("v.PriorityError", ''); 
        } 
        if(Status == '' || Status =='--Select--' || Status == 'undefined' || Status == null) {
            errorFlag = 'true';
            component.set("v.StatusError", 'Input field required'); 
        } else { 
            component.set("v.StatusError", ''); 
        } 
        if(Subject == '' || Subject =='--Select--' || Subject == 'undefined' || Subject == null) {
            errorFlag = 'true';
            component.set("v.SubError", 'Input field required'); 
        } else { 
            component.set("v.SubError", ''); 
        } 
        if(Description == '' || Description =='--Select--' || Description == 'undefined' || Description == null) {
            errorFlag = 'true';
            component.set("v.decError", 'Input field required'); 
        } else { 
            component.set("v.decError", ''); 
        } 
        if(errorFlag == 'false') { 
            var action = component.get('c.savecase');  
            action.setParams({
                "UM" : component.get("v.UM"),
                "User" : localStorage.getItem("IDTenant")
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.helpdec",true);
                    component.set("v.Submitrequest", false);
                   
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Submit Request has been successfully sent',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                     $A.get('e.force:refreshView').fire();
                    
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
        },
    })