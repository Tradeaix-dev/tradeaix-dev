({
    ForgotPwd : function(component, event, helper) {
        debugger;   
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        };
        
        component.set("v.setTenant",getUrlParameter('tenant'));  
          
        var Action = component.get("c.getTenant"); 
        Action.setParams({
            "TenantId" : component.get("v.setTenant")               
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.LogoURL",results.Userlists.Tenant_Logo_Url__c);
            }
        });
        $A.enqueueAction(Action);
       
        component.set("v.setUserName",getUrlParameter('username'));
        var Action1 = component.get("c.getUsername");
            Action1.setParams({
                "username" : component.get("v.setUserName")
            });
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                   component.set("v.UserName",results.Userlists.User_Name__c);
                   component.set("v.lastpasswordupdate",results.Userlists.Last_Password_Change_or_Reset__c);
                }
            });
            $A.enqueueAction(Action1);
    },    
	updatePassword : function(component, event, helper) {
        debugger;    
        var newPassword = component.find("newPassword").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");       
        var re = new RegExp("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
        if(newPassword == "" || confirmPassword == ""){
            component.set("v.validationvalue",true);
            component.set("v.validation",false);
            component.set("v.combinationvalidation",false);
        }
        else if(newPassword != confirmPassword){
            component.set("v.validation",true);
            component.set("v.validationvalue",false);
            component.set("v.combinationvalidation",false);
        }
        else if (!re.test(confirmPassword)) {
            component.set("v.combinationvalidation",true);
            component.set("v.validation",false);
            component.set("v.validationvalue",false);
        } 
            
            else{
            var sessionname = localStorage.getItem("UserSession");
            var Action1 = component.get("c.forgotPasswordUpdate");
            
            Action1.setParams({
                "username" : component.get("v.setUserName"),               
                "newpassword": newPassword
            });
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location='/'+site+'/s/'+component.get("v.setTenant");
                }
            });
            $A.enqueueAction(Action1);
        }
    },
    
    cancel:function(component, event, helper) {
        debugger;
       var site = $A.get("{!$Label.c.Org_URL}");
       window.location='/'+site+'/s/'+component.get("v.setTenant");
    }

})