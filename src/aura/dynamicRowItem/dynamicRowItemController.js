({
    AddNewRow : function(component, event, helper){
        component.getEvent("AddRowEvt").fire();     
    },
     Save: function(component, event, helper) {
        debugger;
        // first call the helper function in if block which will return true or false.
        // this helper function check the "first Name" will not be blank on each row.
        //if (helper.validateRequired(component, event)) {
            // call the apex class method for save the Contact List
            // with pass the contact List attribute to method param.  
            var action = component.get("c.saveContacts");
            action.setParams({
                "ListContact": component.get("v.ContactInstance")
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // if response if success then reset/blank the 'contactList' Attribute 
                    // and call the common helper method for create a default Object Data to Contact List 
                    component.set("v.contactList", []);
                    //helper.createObjectData(component, event);
                   // alert('record Save');
                }
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
       // }
    },
    removeRow : function(component, event, helper){
        debugger;
        var action = component.get('c.DeleteERs');  
        //alert(component.get("v.SelectedRule"));
            action.setParams({ 
                "Uid" :component.find('selectEnginerrole').get("v.value"),
                "SelectedRule" : component.get("v.SelectedRule1")
            }); 
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    
                }
            }); 
            $A.enqueueAction(action);
       component.getEvent("DeleteRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }, 
    changeEnginerole : function(component, event) {
                    var cmpTarget = component.find('selectEnginerrole').get("v.value");
                    //alert(cmpTarget);
                    debugger;
        if(cmpTarget =='Party Validator' || cmpTarget =='Party Approver'){
            component.set("v.ContactInstance",'');  
        }
            var action = component.get('c.ADDERs');  
            action.setParams({ 
                "bankId" : component.get('v.bankId'),
                "RoleName" : cmpTarget
            }); 
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    component.set("v.SelectedRule1",result);
                }
            }); 
            $A.enqueueAction(action);
        $A.enqueueAction(component.get('c.addNewRow1'));
    },
    
 addNewRow1 : function(component, event) {
     //alert('inside addnew1');
    var action = component.get('c.GetBankRule');  
            action.setParams({ 
                "bankId" : component.get('v.bankId')
            }); 
            action.setCallback(this, function(actionResult) {  
                var state = actionResult.getState(); 
                if(state === "SUCCESS"){
                    var result = actionResult.getReturnValue();
                    var cmpTarget = component.find('selectEnginerrole').get("v.value");
                   // alert(cmpTarget);
                    debugger;
                    if(cmpTarget =='Party Validator'){ 
                        
                        var RowItemList = component.get("v.ContactInstance");
                        RowItemList.push(result.CUUsersval);
                        component.set("v.ContactInstance", RowItemList);
                       
                    } else if(cmpTarget =='Party Approver'){ 
                         var RowItemList = component.get("v.ContactInstance");
                        RowItemList.push(result.CUUsersapp1);
                        component.set("v.ContactInstance", RowItemList);                       
                    }
                    else if(cmpTarget =='Comment Role'){ 
                         var RowItemList = component.get("v.ContactInstance");
                        RowItemList.push(result.CUUserscom);
                        component.set("v.ContactInstance", RowItemList);                       
                    }
                   // alert(JSON.stringify(component.get("v.ContactInstance")));
                    //$A.enqueueAction(component.get('c.addNewRow1'));
                }
            }); 
            $A.enqueueAction(action);
    },
    removeDeletedRow1: function(component, event, helper) {
        debugger;
        //alert(index);
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        //alert(index);
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = component.get("v.ContactInstance");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        component.set("v.ContactInstance", AllRowsList);
    },
})