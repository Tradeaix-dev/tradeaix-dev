({
    setGridOption:function(component, result) {
        debugger;
        
        var allValues = result;
        console.log('allValues--->' + allValues);
        var objectValue = allValues.sObjectDatastr;
        console.log('objectValue--->' + objectValue);
        component.set("v.total_size",objectValue.length);
        var fieldList = allValues.fieldList;
        console.log('fieldList--->' + fieldList);
        component.set("v.resHeader",fieldList);
        component.set("v.resbody",objectValue);
        /* Create Dynamic Table */
        var sObjectDataTableHeader = [];
        // Create table Header
        for (var i=0; i<fieldList.length; i++) {
            sObjectDataTableHeader.push(fieldList[i].label);
        }
        console.log('sObjectDataTableHeader--->>' + sObjectDataTableHeader);
        //Get the count of columns.
        var columnCount = sObjectDataTableHeader.length;
        //Create a HTML Table element.
        var table = document.createElement("TABLE");
        //table.border='2';
        //Add the header row.
        var row = table.insertRow(-1);
        for (var i=0; i<columnCount; i++) {
            var headerCell = document.createElement("TH");
            //headerCell.width='75';
            headerCell.innerHTML = sObjectDataTableHeader[i];
            headerCell.className='headerClass';
            row.appendChild(headerCell);
        }
        var dvTable = document.getElementById("sfdctable");
        dvTable.innerHTML = "";
        dvTable.appendChild(table);
        var noofcall = 0;
        debugger;
        /* Create Dynamic Table End */    
        if(objectValue.length){
            for(var j=0; j<objectValue.length; j++){
                // Dynamic table Row
                row = table.insertRow(-1);
                // Dynamic Table Row End
                for (var i=0; i<fieldList.length; i++) {
                    // Dynamic table Row
                    /*if(objectValue[j].split("|")[i] == undefined){ 
                        var cell = row.insertCell(-1);   
                        cell.innerHTML = '-'; 
                    }
                    else{*/
                    var flag=false;
                    for (var k=0; k<objectValue[j].split("|").length; k++) {
                        if(fieldList[i].label == objectValue[j].split("|")[k].split(':')[0]){
                            if(objectValue[j].split("|")[k].split(':')[1]=='null' || objectValue[j].split("|")[k].split(':')[1]==undefined)
                            {flag = true;
                             var cell = row.insertCell(-1); 
                             cell.innerHTML = '-';
                            }else{
                                flag = true;
                                var cell = row.insertCell(-1);  
                                cell.innerHTML = objectValue[j].split("|")[k].split(':')[1]; 
                            }
                        }
                        else{
                            
                        }
                    }
                    if(flag==false){                          
                        var cell = row.insertCell(-1);   
                        cell.innerHTML = '-';
                    }
                    // } 
                    
                    
                }
            }
        }
    },        	         
    // ## function call on Click on the "Download As CSV" Button. 
    
    convertArrayOfObjectsToCSV : function(component,Header,Data){
        debugger;
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        //if (objectRecords == null || !objectRecords.length) {
        //    return null;
        // }
        var objectRecords='';
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        var strheader='';
        for (var i=0; i<Header.length; i++) {
            strheader +=Header[i].label+',';
        }
        //alert(strheader);
        csvStringResult = '';
        csvStringResult += strheader;
        csvStringResult += lineDivider;
        for(var j=0; j<Data.length; j++){
            for (var i=0; i<Header.length; i++) {
                
                
                var flag=false;
                for (var k=0; k<Data[j].split("|").length; k++) {
                    if(Header[i].label == Data[j].split("|")[k].split(':')[0]){
                        if(Data[j].split("|")[k].split(':')[1]=='null' || Data[j].split("|")[k].split(':')[1]==undefined)
                        {flag = true;
                         csvStringResult += '-,';
                        }else{
                            flag = true;
                            csvStringResult += Data[j].split("|")[k].split(':')[1]+',';
                        }
                    }
                    else{
                        
                    }
                }
                if(flag==false){ 
                    csvStringResult += '-,';
                }
                
            }
            csvStringResult += lineDivider;
        }
        //alert(csvStringResult);
        return csvStringResult;        
    },
})