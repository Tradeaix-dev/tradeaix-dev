({
    doInit : function(component, event, helper) {
        if(localStorage.getItem("LoggeduserTenantID") =='a0N6C000000fQDgUAM')
        {
            component.set("v.TID",false);
        }else{
            component.set("v.TID",true);
        }
        debugger;
        var opts = [];
        opts = [ 
            { label: "All", value: "All"},
            { label: "Party", value: "Party", selected: "true"},
            { label: "Counterpartry", value: "Counterpartry" },
            
        ];  
            component.set("v.filterOptions", opts);           
            component.set("v.selectedItem", "All");  
            
            component.set("v.Filtericon", false);     
            var action = component.get("c.Initcall");
            action.setParams({
            "selectedValue" : 'All',
            "TenantId" : localStorage.getItem("LoggeduserTenantID"),
            "selected" : "",
            "PTempt" :""
            });
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
            debugger;
            helper.setGridOption(component, response.getReturnValue());
            }
            }); 
            $A.enqueueAction(action);
            },
            backtoreport: function (component, event, helper){
            var site = $A.get("{!$Label.c.Org_URL}");
            window.location = '/'+site+'/s/reports';
            
            //window.location='https://tradeaix45-cmfg.cs63.force.com/dev/s/report/Report/Recent?queryScope=mru';
            debugger;
            },
            
downloadCsv : function(component,event,helper){
            debugger;
            // get the Records [contact] list from 'ListOfContact' attribute 
            var Header = component.get("v.resHeader");
            var Data = component.get("v.resbody");
            // call the helper function which "return" the CSV data as a String   
            var csv = helper.convertArrayOfObjectsToCSV(component,Header,Data);   
            if (csv == null){return;} 
            
            // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
            hiddenElement.target = '_self'; // 
            var d = new Date();
            hiddenElement.download = 'Transaction '+d+'.csv';  // CSV file Name* you can change it.[only name not .csv] 
            document.body.appendChild(hiddenElement); // Required for FireFox browser
            hiddenElement.click(); // using click() js function to download csv file
            }, 
            ViewFilter : function(component, event, helper) {
            debugger;        
            var selected = event.getSource().get("v.text");
            var action = component.get("c.Viewcall");
            component.set("v.selectedItem", selected);  
            action.setParams({
            "selectedValue" : selected,
            "TenantId" : 'a0f0m0000003vXB',
            "selected" : "",
            "PTempt" :""
            });
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
            debugger;
            helper.setGridOption(component, response.getReturnValue());
            }
            }); 
            $A.enqueueAction(action);
            },
            doSearch : function(component, event, helper) {
            debugger;        
            var SearchKey = component.find("searchtxtorg").get("v.value");
            var action = component.get("c.Searchcall");
            //component.set("v.selectedItem", selected);  
            action.setParams({
            "selectedValue" : '',
            "TenantId" : '',
            "selected" : SearchKey
            });
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
            debugger;
            helper.setGridOption(component, response.getReturnValue());
            }
            }); 
            $A.enqueueAction(action);
            },
            btnFilter:function(component, event, helper) {
            component.set("v.Filter", true);
            debugger;
            var action = component.get("c.bindfilterres");
            action.setParams({
            "UID" : localStorage.getItem("IDTenant")
            });
            action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
            debugger;
            var res =response.getReturnValue();
            if(res.length==0 )
            {
            component.set("v.resetfilter",false);
            }else{
            component.find("searchtxtorg").set("v.value",res[0].Keywords__c);
        component.find("txtnumberfrom").set("v.value",res[0].TransactionRefNumberFrom__c);
        component.find("txtnumberto").set("v.value",res[0].TransactionRefNumberTo__c);
        component.find("txtamountfrom").set("v.value",res[0].AmountFrom__c);
        component.find("txtamountto").set("v.value",res[0].AmountTo__c);
        component.find("txtlcnumberfrom").set("v.value",res[0].LCNumberFrom__c);
        component.find("txtlcnumberto").set("v.value",res[0].LCNumberTo__c);
        component.find("expiryddatefrom").set("v.value",res[0].Expired_Date_From__c);
        component.find("expiryddateto").set("v.value",res[0].Expired_Date_To__c);
        component.find("createdfrom").set("v.value",res[0].CreateddateFrom__c);
        component.find("createdto").set("v.value",res[0].CreateddateTo__c);
        component.find("txtstatus").set("v.value",res[0].Transaction_Status__c);
        debugger;
         component.find("txtQstatus").set("v.value",res[0].Quote_Status__c);
         component.find("txtQamountfrom").set("v.value",res[0].QuoteAmtFrom__c);
         component.find("txtQamountto").set("v.value",res[0].QuoteAmtTo__c);
         component.find("txtQFamountfrom").set("v.value",res[0].QuoteFAmtFrom__c);
         component.find("txtQFamountto").set("v.value",res[0].QuoteFAmtTo__c);
         component.find("txtcpname").set("v.value",res[0].CounterParty__c);
        
        component.set("v.resetfilter",true);
    }
}
 }); 
$A.enqueueAction(action);

var CP = component.get("c.getCP");

CP.setCallback(this, function(response) {
    debugger;
    var state = response.getState();
    if (component.isValid() && state == 'SUCCESS') {   
        var CP = response.getReturnValue();
        var options = [];
         options = [{ label: "--Select--", value: "--Select--"}];       
        CP.forEach(function(CPdata)  {
            options.push({ value: CPdata.Tenant_Approved__r.Id, label: CPdata.Tenant_Approved__r.Tenant_Site_Name__c});
        });
        component.set("v.TName", options);
    } else {
        console.log('Failed with state: ' + state);
    }
});
$A.enqueueAction(CP); 

var CPT = component.get("c.getPTemplate");
debugger;
CPT.setParams({
    "TenantId" : localStorage.getItem("LoggeduserTenantID")
});
        CPT.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state == 'SUCCESS') {   
                var CPT = response.getReturnValue();
                var options = [];
                CPT.forEach(function(CPdata)  { 
                    //alert(CPdata.Id);
                    options.push({ value: CPdata.Id, label: CPdata.TemplateName__c});
                    
                });
                component.set("v.listOptions", options);
            } else {
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(CPT); 

},
    cancelfilter : function(component, event, helper) {
        debugger; 
        component.set("v.Filter", false);    
    },
        applyfilter: function(component, event, helper) {
            debugger;
            var errorFlag = 'false';
            var regPhoneNo = /^[0-9]*$/;
            component.set("v.Filtericon", true);
            var trannofrom = component.find("txtnumberfrom").get("v.value"); 
            var trannoto = component.find("txtnumberto").get("v.value");
            
            var amountfrom = component.find("txtamountfrom").get("v.value");
            var amountto = component.find("txtamountto").get("v.value");
            
            var expdatefrom = component.find("expiryddatefrom").get("v.value");
            var expdateto = component.find("expiryddateto").get("v.value");
            
            var lcnumberfrom = component.find("txtlcnumberfrom").get("v.value");
            var lcnumberto = component.find("txtlcnumberto").get("v.value");
            
            var createdfrom = component.find("createdfrom").get("v.value"); 
            var createdto = component.find("createdto").get("v.value");
            var SrchKey = component.find("searchtxtorg").get("v.value");
            if(trannofrom == undefined && trannoto == undefined) { 
                component.set("v.transnumberError", ''); 
            } 
            else if((trannofrom != ''  && (trannoto == '' || trannoto == undefined || trannoto == null)) || (trannoto != ''&& (trannofrom == '' || trannofrom == undefined || trannofrom == null) )) {
                errorFlag = 'true';
                component.set("v.transnumberError", 'Transaction Number	should not be blank.'); 
            } else { 
                component.set("v.transnumberError", ''); 
            } 
            if(amountfrom == undefined && amountto == undefined) { 
                component.set("v.AmountError", ''); 
            } 
            else if((amountfrom != ''  && (amountto == '' || amountto == undefined || amountto == null)) || (amountto != ''&& (amountfrom == '' || amountfrom == undefined || amountfrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.AmountError", 'Amount should not be blank.'); 
            }
                else  if(!amountfrom.match(regPhoneNo) || !amountto.match(regPhoneNo))
                {
                    
                    errorFlag = 'true';
                    component.set("v.AmountError", 'Amount should be only number'); 
                } 
                    else { 
                        component.set("v.AmountError", ''); 
                    } 
            
            if(lcnumberfrom == undefined && lcnumberto == undefined) { 
                component.set("v.lcnumberError", ''); 
            } 
            else if((lcnumberfrom != ''  && (lcnumberto == '' || lcnumberto == undefined || lcnumberto == null)) || (lcnumberto != ''&& (lcnumberfrom == '' || lcnumberfrom == undefined || lcnumberfrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.lcnumberError", 'LC Number	should not be blank.'); 
            }
                else  if(!lcnumberfrom.match(regPhoneNo) || !lcnumberto.match(regPhoneNo))
                {
                    
                    errorFlag = 'true';
                    component.set("v.AmountError", 'LC Number should be only number'); 
                } 
                    else { 
                        component.set("v.lcnumberError", ''); 
                    } 
            if(expdatefrom == undefined && expdateto == undefined) { 
                component.set("v.ExpiryError", ''); 
            } 
            else if((expdatefrom != ''  && (expdateto == '' || expdateto == undefined || expdateto == null)) || (expdateto != ''&& (expdatefrom == '' || expdatefrom == undefined || expdatefrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.ExpiryError", 'Expiry Date	should not be blank.'); 
            } else { 
                component.set("v.ExpiryError", ''); 
            } 
            if(createdfrom == undefined && createdto == undefined) { 
                component.set("v.createddateError", ''); 
            } 
            else if((createdfrom != ''  && (createdto == '' || createdto == undefined || createdto == null)) || (createdto != ''&& (createdfrom == '' || createdfrom == undefined || createdfrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.createddateError", 'Created Date should not be blank.'); 
            } else { 
                component.set("v.createddateError", ''); 
            } 
            var Qamountfrom = component.find("txtQamountfrom").get("v.value");
            var Qamountto = component.find("txtQamountto").get("v.value");
            var QFamountfrom = component.find("txtQFamountfrom").get("v.value");
            var QFamountto = component.find("txtQFamountto").get("v.value");
                        
            if(Qamountfrom == undefined && Qamountto == undefined) { 
                component.set("v.QAmountError", ''); 
            } 
            else if((Qamountfrom != ''  && (Qamountto == '' || Qamountto == undefined || Qamountto == null)) || (Qamountto != ''&& (Qamountfrom == '' || Qamountfrom == undefined || Qamountfrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.QAmountError", 'Quote Amount should not be blank.'); 
            }
                else  if(!Qamountfrom.match(regPhoneNo) || !Qamountto.match(regPhoneNo))
                {
                    
                    errorFlag = 'true';
                    component.set("v.QAmountError", 'Quote Amount should be only number'); 
                } 
                    else { 
                        component.set("v.QAmountError", ''); 
                    } 
            
            if(QFamountfrom == undefined && QFamountto == undefined) { 
                component.set("v.QFAmountError", ''); 
            } 
            else if((QFamountfrom != ''  && (QFamountto == '' || QFamountto == undefined || QFamountto == null)) || (QFamountto != ''&& (QFamountfrom == '' || QFamountfrom == undefined || QFamountfrom == null) )) {
                
                errorFlag = 'true';
                component.set("v.QFAmountError", 'Quote Fee (bps) should not be blank.'); 
            }
                else  if(!QFamountfrom.match(regPhoneNo) || !QFamountto.match(regPhoneNo))
                {
                    
                    errorFlag = 'true';
                    component.set("v.QFAmountError", 'Quote Fee (bps) should be only number'); 
                } 
                    else { 
                        component.set("v.QFAmountError", ''); 
                    } 
                   
                        
            if(errorFlag=='false'){
                var action = component.get("c.filtercall");
                action.setParams({
                    "UID" : localStorage.getItem("IDTenant"),
                    "TenantId" : localStorage.getItem("LoggeduserTenantID"),
                    "transrange" : trannofrom+":"+trannoto,
                    "amtrange" : amountfrom+":"+amountto,
                    "Expdaterange" : expdatefrom+":"+expdateto,
                    "lcnumberrange" : lcnumberfrom+":"+lcnumberto,
                    "createdrange" : createdfrom+":"+createdto,
                    "SearchKey" :  SrchKey,
                    "TStatus" : component.find("txtstatus").get("v.value"),
                    "CPName" :  component.find("txtcpname").get("v.value"),
                    "QStatus" :  component.find("txtQstatus").get("v.value"),
                    "Qamtrange" : Qamountfrom+":"+Qamountto,
                    "QFamtrange" : QFamountfrom+":"+QFamountto,
                    "PTempt" : component.get("v.selectedOptions")
                    
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set("v.Filter", false);
                        helper.setGridOption(component, response.getReturnValue());
                    }
                });
                $A.enqueueAction(action);
            }
        },
                    handleChange: function (component, event, helper) {
        debugger;
        var selectedFields = event.getParam("value");
        var targetName = event.getSource().get("v.name");
        if(targetName == 'srcFields'){ 
            component.set("v.selectedOptions", selectedFields);
        }
    },
            resetfilter: function(component, event, helper) {
                debugger;  
                component.find("searchtxtorg").set("v.value",''); 
                component.find("txtnumberfrom").set("v.value",''); 
                component.find("txtnumberto").set("v.value",''); 
                component.find("txtamountfrom").set("v.value",''); 
                component.find("txtamountto").set("v.value",''); 
                component.find("expiryddatefrom").set("v.value",''); 
                component.find("expiryddateto").set("v.value",''); 
                component.find("txtlcnumberfrom").set("v.value",''); 
                component.find("txtlcnumberto").set("v.value",'');
                component.find("createdfrom").set("v.value",''); 
                component.find("createdto").set("v.value",''); 
                component.set("v.transnumberError", '');
                component.set("v.AmountError", ''); 
                component.set("v.lcnumberError", ''); 
                component.set("v.ExpiryError", '');
                component.set("v.createddateError", ''); 
                component.set("v.selectedOptions",'');
                
                component.find("txtQstatus").set("v.value",''); 
                component.find("txtQamountfrom").set("v.value",''); 
                component.find("txtQamountto").set("v.value",'');
                component.find("txtQFamountfrom").set("v.value",''); 
                component.find("txtQFamountto").set("v.value",''); 
                component.find("txtcpname").set("v.value",''); 
                component.find("txtstatus").set("v.value",''); 
                var action = component.get("c.resetfiltercall");
                action.setParams({
                    "UID" : localStorage.getItem("IDTenant")
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        component.set("v.resetfilter",false);
                        debugger;
                    }}); 
                $A.enqueueAction(action);
                
                var a = component.get('c.doInit');
                $A.enqueueAction(a);
            }
})