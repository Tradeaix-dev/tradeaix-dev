({
    getOrglist : function(component, event, helper) {
        debugger;
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }catch(ex){}
        }; 
        var orgId = getUrlParameter('orgid'); 
        component.set("v.OrgId",orgId);
        var Action = component.get("c.getOrganizationdetails");
        var sessionname = localStorage.getItem("UserSession");   
        Action.setParams({
            "organizationId" : orgId,
            "username" : sessionname
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state == "SUCCESS"){
                component.set("v.Org",results.organization);
            }
        });
        $A.enqueueAction(Action);
    },
    
    saveOrganization: function(component, event, helper){
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        var orgnameval = component.find("orgnameval").get("v.value");
        var orgindval = component.find("orgindval").get("v.value");
        var OrgRevenueval = component.find("OrgRevenueval").get("v.value");
        var orgpWebsiteval = component.find("orgpWebsiteval").get("v.value");
        var orgEmployeesval = component.find("orgEmployeesval").get("v.value");
        var orgLogoval = component.find("orgLogoval").get("v.value");
        var orgconnameval = component.find("orgconnameval").get("v.value");
        var orgconemailval = component.find("orgconemailval").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        var orgphoneval = component.find("orgphoneval").get("v.value");
        var OrgPconphoneval = component.find("OrgPconphoneval").get("v.value");
      /*  if(!$A.util.isEmpty(orgphoneval))
        {   
            
            if(!orgphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgphonemsg", 'Please enter valid phone number'); 
            } 
            else if(orgphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.orgphonemsg", ''); 
                }
        }
        if(!$A.util.isEmpty(OrgPconphoneval))
        {   
            
            if(!OrgPconphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.OrgPconphonemsg", 'Please enter valid phone number'); 
            } 
            else if(OrgPconphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.OrgPconphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.OrgPconphonemsg", ''); 
                }
        }
        if(orgnameval == undefined || orgnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.orgnamemsg",'Input field required');
        }
        else {
            component.set("v.orgnamemsg",'');
        } 
        if(orgindval == undefined || orgindval == "--Select--" || orgindval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Industrymsg",'Input field required');
        }
        else{
            component.set("v.Industrymsg",'');            
        } if(OrgRevenueval == undefined || OrgRevenueval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Revenuemsg",'Input field required');
        }
        else if(!OrgRevenueval.match(regPhoneNo))
        {
            
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Revenuemsg", 'Please enter only number'); 
        } 
            else {
                component.set("v.Revenuemsg",'');
            }if(orgpWebsiteval == undefined || orgpWebsiteval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.WebsiteMsg",'Input field required');
            }
        else {
            component.set("v.WebsiteMsg",'');
        }if(orgEmployeesval == undefined || orgEmployeesval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Employeesmsg",'Input field required');
        }else if(!orgEmployeesval.match(regPhoneNo))
        {
            
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Employeesmsg", 'Please enter only number'); 
        } 
            else{
                component.set("v.Employeesmsg",'');
            } if(orgLogoval == undefined || orgLogoval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orglogomsg",'Input field required');
            }
        else {
            component.set("v.orglogomsg",'');
        }if(orgconnameval == undefined || orgconnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.ContnameMsg",'Input field required');
        }
        else{
            component.set("v.ContnameMsg",'');
        } if(orgconemailval == undefined || orgconemailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.orgemailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(orgconemailval))
        {   
            if(!orgconemailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.orgemailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.orgemailmsg", ''); 
            }
        }else{
            component.set("v.orgemailmsg",'');
        }  */
        if(errorFlag == 'false') { 
            var Action = component.get("c.updateOrganization");
            var sessionname = localStorage.getItem("UserSession");   
            Action.setParams({
                "LoggedUserID" : localStorage.getItem("UserSession"),
                "Organization" : component.get("v.Org"),
                "username" : sessionname
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state == "SUCCESS"){
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/administration';
                }
            });
            
        }
        $A.enqueueAction(Action);
    },
    
    cancelOrg : function(component, event, helper){
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}"); 
        window.location = '/'+site+'/s/organizationview?orgid='+component.get("v.OrgId");
    }
    
    
})