({
    getUserList : function(component, event, helper) {
        debugger;
        if(localStorage.getItem('LoggeduserProfile') == $A.get("{!$Label.c.ACP_ProfileID}"))
        {
            component.set("v.Tenantcheck",true);
            var Action1 = component.get("c.loadorgs");   
            Action1.setParams({
                "username" : localStorage.getItem('UserSession')                
            });        
            Action1.setCallback(this, function(actionResult1) {
                debugger;
                var state = actionResult1.getState(); 
                var results = actionResult1.getReturnValue();
                if(state === "SUCCESS"){
                    component.set("v.OrgFilter",results);
                }
                
            }); 
            $A.enqueueAction(Action1);
        }
        else{
            component.set("v.Tenantcheck",false);                        
        }
        
        
        debugger;
        var Action = component.get("c.getUserprofile");
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");
        Action.setParams({
            "username" : sessionname
        });
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){                
                component.set("v.Users",results.userResults);
                component.set("v.Users1",results.userResults1);
                component.set("v.superAdmin",results.isAdmin);
                
            }
        });
        $A.enqueueAction(Action);
    },
    addUser : function(component, event, helper) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/user-create';
    },
    
    searchUser : function(component, event, helper) {
        debugger;
        var searchtext = component.find("searchtxt").get("v.value");  
        var errorFlag = 'false';
        if(searchtext == '' || searchtext == null || searchtext == undefined || searchtext.trim().length*1 == 0) 
        {  
            errorFlag = 'true';
            component.set("v.txtusersearch", 'Input field required'); 
        }
        else{  
            component.set("v.txtusersearch", ''); 
        }
        if(errorFlag == 'false')
        { 
            var Action = component.get("c.getUserSearch");        
            var sessionname = localStorage.getItem("UserSession");
            Action.setParams({
                "username" : sessionname,
                "seachtext" : searchtext
            });
            Action.setCallback(this, function(actionResult) {
                debugger;
                var state = actionResult.getState(); 
                var results = actionResult.getReturnValue();
                if(state === "SUCCESS"){               
                    component.set("v.Users",results.userResults);                
                }
            });
            $A.enqueueAction(Action);
        }
    },
    onSingleSelectChange : function(component, event, helper){
         debugger;
        var selectCmp = component.find("InputSelectSingle").get("v.value"); 
        component.set("v.userid",localStorage.getItem("UserSession"));
        var sessionname = localStorage.getItem("UserSession");          
        if(selectCmp == 'All Realms'){
            var Action = component.get("c.getUserprofile");
            component.set("v.userid",localStorage.getItem("UserSession"));
            var sessionname = localStorage.getItem("UserSession");
            Action.setParams({
                "username" : sessionname
            });
        }else{
            var Action = component.get("c.getfilterUserList"); 
            Action.setParams({           
                "tenantId" : selectCmp
            });
        }
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.Users",results.userResults); 
                component.set("v.superAdmin",true);
                
            }
        });
        $A.enqueueAction(Action);
    },
    addTaskassignee: function(component, event, helper){
        debugger;
           var Action = component.get("c.getCompletedTransactions"); 
            Action.setParams({           
                "tenantId" : localStorage.getItem('LoggeduserTenantID')
            });
       
        Action.setCallback(this, function(actionResult) {
            debugger;
            var state = actionResult.getState(); 
            var results = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.completedTransaction", results);  
                var selectedTemplateId = results[1].value; 
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/taskassignee?transactionId='+selectedTemplateId+'&flag=TA';
            }
        });
        $A.enqueueAction(Action);
        //component.set("v.createTask", true);
    },
    canceltaskassignee: function(component, event, helper){
        debugger;
        component.set("v.createTask", false);
    },
    createTaskassignee : function(component, event, helper){
        debugger;
        var selectedTemplateId = component.find("transrefnumber").get("v.value"); 
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/counterpartynew?transactionId='+selectedTemplateId+'&flag=TA';
        
    }
})