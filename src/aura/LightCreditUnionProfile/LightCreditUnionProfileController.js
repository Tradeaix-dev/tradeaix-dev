({
    getCuProfile : function(component, event, helper) {		         
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                debugger;
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        }; 
        var creditUnionId = getUrlParameter('cuId'); 
        
        component.set("v.creditUnionId", creditUnionId); 
        var action = component.get('c.GetCreditUnionProfile');  
        action.setParams({
            "creditUnionId" : component.get("v.creditUnionId")
        });
        action.setCallback(this, function(actionResult) {  
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.CreditUnionProfile", result);
                component.set("v.creditUnionId", result.CreditUnion.Id);
                component.set("v.displayAdmin",result.displayAdmin);
            }
        }); 
        $A.enqueueAction(action);
	},
    gotoMessageTab: function(cmp) {
        cmp.find("tabs").set("v.selectedTabId", "messages");
    }
})