({
    getIB : function(component, event, helper) {
    
    var sitename = localStorage.getItem("TSiteName").charAt(0).toUpperCase() + localStorage.getItem("TSiteName").slice(1)
            document.title =sitename +' : Add a New Issuing Bank';
    },
    
    saveIssuingbank : function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        var bankname = component.find("Banknameval").get("v.value");
        var swiftcode = component.find("swiftcode").get("v.value");
        var primarycontactname = component.find("contactname").get("v.value");
        var primarycontactemail = component.find("contactemail").get("v.value");
        var website = component.find("website").get("v.value");
       //var logourl = component.find("logourl").get("v.value");
        var contactmobile = component.find("contactmobile").get("v.value");
        var street = component.find("street").get("v.value");
        var city = component.find("city").get("v.value");
        var state = component.find("state").get("v.value");
        var zipcode = component.find("zipcode").get("v.value");
        var country = component.find("country").get("v.value");
        var countrycode = component.find("auracountrycode").get("v.value");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        if(bankname == '' || bankname == undefined || bankname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        } else { 
            component.set("v.nameError", ''); 
        }
        if(swiftcode == '' || swiftcode == undefined || swiftcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.swiftcodeError", 'Input field required'); 
        } else { 
            component.set("v.swiftcodeError", ''); 
        }
        if(primarycontactname == '' || primarycontactname == undefined || primarycontactname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.contactnameError", 'Input field required'); 
        } else { 
            component.set("v.contactnameError", ''); 
        }
        if(primarycontactemail == '' || primarycontactemail == undefined || primarycontactemail == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.contactemailError", 'Input field required'); 
        }else if(!$A.util.isEmpty(primarycontactemail)){   
            if(!primarycontactemail.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.contactemailError", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.contactemailError", ''); 
            }
        }
        if(website == '' || website == undefined || website == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.websiteError", 'Input field required'); 
        } else { 
            component.set("v.websiteError", ''); 
        }
       /* if(logourl == '' || logourl == undefined || logourl == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.logourlError", 'Input field required'); 
        } else { 
            component.set("v.logourlError", ''); 
        } */
        if(contactmobile == '' || contactmobile == undefined || contactmobile == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.contactmobileError", 'Input field required'); 
        } else { 
            component.set("v.contactmobileError", ''); 
        } 
        if(!$A.util.isEmpty(contactmobile))
        {   
            if(countrycode == '' || countrycode =='--Select--' || countrycode == 'undefined' || countrycode == null) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.countrycodeError", 'Input field required'); 
            } else { 
                component.set("v.countrycodeError", ''); 
            } 
            
            if(!contactmobile.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.contactmobileError", 'Please enter valid phone number'); 
            } 
            else if(contactmobile.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.contactmobileError", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.contactmobileError", ''); 
                }
        }        
       
        if(street == '' || street == undefined || street == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.streetError", 'Input field required'); 
        } else { 
            component.set("v.streetError", ''); 
        }
        if(city == '' || city == undefined || city == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cityError", 'Input field required'); 
        } else { 
            component.set("v.cityError", ''); 
        }
        if(state == '' || state == undefined || state == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.stateError", 'Input field required'); 
        } else { 
            component.set("v.stateError", ''); 
        }
        if(zipcode == '' || zipcode == undefined || zipcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.zipcodeError", 'Input field required'); 
        } else { 
            component.set("v.zipcodeError", ''); 
        }
        if(country == '' || country == undefined || country == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.countryError", 'Input field required'); 
        } else { 
            component.set("v.countryError", ''); 
        }
        if(errorFlag == 'false') {            
            var action = component.get('c.SaveIssuingBank');  
            var sessionname = localStorage.getItem("UserSession");
            action.setParams({
                "LoggedUserID" : sessionname,
                "TenantDetails" : component.get("v.Tenant")
                
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Issuing Bank Sucessfully Created',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();  
                    var site = $A.get("{!$Label.c.Org_URL}");
                    window.location = '/'+site+'/s/issuing-banks';
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
        
    },
    
   /* scriptsLoaded : function(component, event, helper) {
        debugger;
        $(document).ready(function(){ 
            
          //Restrict past date selection in date picker  
            $( "#datepickerId" ).datepicker({
                 
                beforeShowDay: function(date) {
                    var today = new Date();
                    if(date > today){
                        return [true];
                    }
                    else{
                        return [false];
                    }	 
                },
            });           
        }); 
    },*/
    getBanks: function(component, event, helper) { 
        var getUrlParameter = function getUrlParameter(sParam) {
            try{
                var searchString = document.URL.split('?')[1];
                var sPageURL = decodeURIComponent(searchString),
                    sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                    sParameterName,
                    i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');                 
                    if (sParameterName[0] === sParam)  {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            } catch(ex){}
        };
        
        var flagchk = getUrlParameter('flag');
        component.set("v.flagchk", flagchk); 
        
        var TID = getUrlParameter('transactionId');
        component.set("v.TIDs", TID); 
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        component.set("v.currentDate",yyyy+'-'+mm+'-'+dd);
        var ActionTitle = component.get("c.getloggeduserBrowserTitle");
        ActionTitle.setCallback(this, function(actionResultTitle) {
            var statetitle = actionResultTitle.getState(); 
            var resulttitle = actionResultTitle.getReturnValue();
            if(statetitle === "SUCCESS"){
                debugger;
                component.set("v.PartyID",resulttitle.Id);
                component.set("v.Partname",resulttitle.CU_Name__c);
                document.title =resulttitle.Browser_Title__c +' :: Create Issuing Bank';
            }
        });
        $A.enqueueAction(ActionTitle);
        
    },
    popcounterPopupCancel : function(component, event) { 
        debugger;
        component.set("v.newcounterPopup", false);       
    },
    addCounterParties: function(component, event) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false); 
        var Bankname = component.find("Banknameval").get("v.value");
        var vcode =  component.find("code").get("v.value");
        //var address =  component.find("Description__c").get("v.value");
        //var vcity =component.find("city").get("v.value");
        var vpcontact =component.find("pcontact").get("v.value");
        //var vstate =component.find("state").get("v.value");
        
        if(Bankname == '' || Bankname == 'undefined' || Bankname == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cpError", 'Input field required'); 
        } else { 
            component.set("v.cpError", ''); 
        } 
        if(vcode == '' || vcode == 'undefined' || vcode == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.codeerror", 'Input field required'); 
        } else { 
            component.set("v.codeerror", ''); 
        } 
        debugger;
         /*if(address == '' ||address == 'undefined' || address == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.adderror", 'Input field required'); 
        } else { 
            component.set("v.adderror", ''); 
        } 
        if(vcity == '' || vcity == 'undefined'|| vcity == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.cityerror", 'Input field required'); 
        } else { 
            component.set("v.cityerror", ''); 
        } */
        if(vpcontact == '' || vpcontact == 'undefined' || vpcontact == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Pcontacterror", 'Input field required'); 
        } else { 
            component.set("v.Pcontacterror", ''); 
        }
       /* if(vstate == '' || vstate == 'undefined' || vstate == null) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Stateerror", 'Input field required'); 
        } else { 
            component.set("v.Stateerror", ''); 
        } */
        var bankd = component.get("v.bank");
        var actionchk = component.get("c.CheckBankName");
        var bname = bankd.CU_Name__c.trim();
        var bscode = bankd.Swift_Code__c.trim();
        actionchk.setParams({ "bankName": bname,
                             "swiftcode": bscode});
        actionchk.setCallback(this, function(response) {
            var statechk = response.getState();
            var bankchk = response.getReturnValue(); 
            if(bankchk != '' && bankchk != undefined) {
                if(bankchk == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.mainError", 'This Issuing Bank Legal Name and Swift Code has already been used. Please enter a new Issuing Bank Legal Name or Swift Code.');
                } else {
                    component.set("v.mainError", ''); 
                    
                    
                }
            } 
            else { 
                component.set("v.mainError", ''); 
            }
        if(errorFlag == 'false') {
            console.log('@@'+component.get("v.counterpartyname"));
            var action = component.get('c.addCounterPartiesname');  
            action.setParams({
                "saveCreditunion" : component.get("v.bank")
              
            });
            debugger;
            action.setCallback(this, function(actionResult) { 
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Success: ',
                            'message': 'Issuing Bank Sucessfully Created',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                    window.location = '/demo/s/issuing-banks';
                }
            });
            debugger;
            $A.enqueueAction(action);
        }
    
    });
$A.enqueueAction(actionchk);
    },
    onboard: function(component, event) {
        component.set("v.onboard", false);
        var flagchk = component.get("v.flagchk");
        if(flagchk=='P'){
            window.location = '/demo/s/party-bank';
        }else if(flagchk=='C'){
            window.location = '/demo/s/banklist';
        }else if(flagchk=='T'){
            window.location = '/demo/s/transactionviewdetails?transactionId='+component.get("v.TIDs");
        }
    }, 
    Issuingbankcancel:function(component, event) {
        debugger;
        var site = $A.get("{!$Label.c.Org_URL}");
        window.location = '/'+site+'/s/issuing-banks';
    }
})