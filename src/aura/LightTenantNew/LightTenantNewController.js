({
    getTenantList : function(component, event, helper) {
        debugger;
        var action = component.get('c.GetOrg');
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                component.set("v.OrgList",result);
            }
        });
        debugger;
        $A.enqueueAction(action);
    }, 
    cancelTenant: function(component, event, helper) {
       var site = $A.get("{!$Label.c.Org_URL}");
       window.location = '/'+site+'/s/administration';
    },
    saveTenant : function(component, event, helper) {
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);
        
var Tenantnameval = component.find("Tenantnameval").get("v.value");
        
        var Tenantsitenameval = component.find("Tenantsitenameval").get("v.value");
        var Tenanttyppeval = component.find("Tenanttyppeval").get("v.value");
        if(component.get("v.EnableBanktype")){
            var Tenantbanktyppeval = component.find("Tenantbanktyppeval").get("v.value");
        }
        var Tenantdatavisibilityval = component.find("Tenantdatavisibilityval").get("v.value");
        var TenantNewsmsgURLVal = component.find("TenantNewsmsgURLVal").get("v.value");
        var Tenantthkval = component.find("Tenantthkval").get("v.value");
        var Tenantshiftcodeval = component.find("Tenantshiftcodeval").get("v.value");
        var TenantWebsiteval = component.find("TenantWebsiteval").get("v.value");
        var Tanantlandingval = component.find("Tanantlandingval").get("v.value");
        var Tenantlogoval = component.find("Tenantlogoval").get("v.value");
        var Tenantbannerurlval = component.find("Tenantbannerurlval").get("v.value");
        var Tenantnewsmsgval = component.find("Tenantnewsmsgval").get("v.value");
        var TenantPConnameval = component.find("TenantPConnameval").get("v.value");
        var TenantpconEmailval = component.find("TenantpconEmailval").get("v.value");
        var TenantPConphoneval = component.find("TenantPConphoneval").get("v.value");
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var regPhoneNo = /^[0-9]*$/;
        //var onlyletter ='^[0-9]*$'; 
        var onlyletter =/^[A-Za-z]+$/;
        var nameletter =/^[A-Za-z0-9 ]+$/;
        var website =/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

        if(!$A.util.isEmpty(TenantPConphoneval))
        {   
            
            if(!TenantPConphoneval.match(regPhoneNo))
            {
                
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please enter valid phone number'); 
            } 
            else if(TenantPConphoneval.length<10)
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantPConphonemsg", 'Please should be minimum 10 digits'); 
            }
                else{
                    component.set("v.TenantPConphonemsg", ''); 
                }
        }
        if(TenantpconEmailval == undefined || TenantpconEmailval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantpconEmailmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(TenantpconEmailval))
        {   
            if(!TenantpconEmailval.match(regExpEmailformat))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantpconEmailmsg", 'Please Enter a Valid Email Address'); 
            }
            else
            {
                component.set("v.TenantpconEmailmsg", ''); 
            }
        }else{
            component.set("v.TenantpconEmailmsg",'');
        }
        if(Tenantnameval == undefined || Tenantnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantnamemsg",'Input field required');
        } else if(!$A.util.isEmpty(Tenantnameval))
        {   
            if(!Tenantnameval.match(nameletter))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.tenantnamemsg", 'Tenant Name not allowed special characters'); 
            }
            else
            {
                component.set("v.tenantnamemsg", ''); 
            }
        }else{
            component.set("v.tenantnamemsg",'');
        }
        if(Tenantsitenameval == undefined || Tenantsitenameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.tenantsitenamemsg",'Input field required');
        }else{
            if(!$A.util.isEmpty(Tenantsitenameval))
            {   
                debugger;
                
                if(!Tenantsitenameval.match(onlyletter))
                {
                    errorFlag = 'true';
                    component.set("v.validation", true);
                    component.set("v.tenantsitenamemsg",'Tenant Site Name should only be in alphabets.');
                }else{
                    component.set("v.tenantsitenamemsg",'');
                }
            }
        }
        if(Tenanttyppeval == "--Select--" || Tenanttyppeval == undefined || Tenanttyppeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenanttyppeMsg",'Input field required');
        }else{
            component.set("v.TenanttyppeMsg",'');
        }
        if(component.get("v.EnableBanktype")){
            
            if(Tenantbanktyppeval == "--Select--" || Tenantbanktyppeval== undefined || Tenantbanktyppeval == ""){
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantbanktyppemsg",'Input field required');
            }else{
                component.set("v.Tenantbanktyppemsg",'');
            }
        }
        if(Tenantdatavisibilityval == "--Select--" || Tenantdatavisibilityval == undefined || Tenantdatavisibilityval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantdatavisibilitymsg",'Input field required');
        }else{
            component.set("v.Tenantdatavisibilitymsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }
        if(Tenantthkval == undefined || Tenantthkval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantthkmsg",'Input field required');
        }else{
            component.set("v.Tenantthkmsg",'');
        }
        if(Tenantshiftcodeval == undefined || Tenantshiftcodeval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantshiftcodeMsg",'Input field required');
        }else{
            component.set("v.TenantshiftcodeMsg",'');
        }
        if(TenantWebsiteval == undefined || TenantWebsiteval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantWebsitevmsg",'Input field required');
        }
        else if(!$A.util.isEmpty(TenantWebsiteval))
        {   
            if(!TenantWebsiteval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantWebsitevmsg", 'Please Enter a Valid Website'); 
            }
            else
            {
                component.set("v.TenantWebsitevmsg", ''); 
            }
        }
        else{
            component.set("v.TenantWebsitevmsg",'');
        }
        if(Tanantlandingval == undefined || Tanantlandingval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tanantlandingmsg",'Input field required');
        }else{
            component.set("v.Tanantlandingmsg",'');
        }
        if(Tenantlogoval == undefined || Tenantlogoval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantlogomsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantlogoval))
        {   
            if(!Tenantlogoval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantlogomsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.Tenantlogomsg", ''); 
            }
        }else{
            component.set("v.Tenantlogomsg",'');
        }
        if(Tenantbannerurlval == undefined || Tenantbannerurlval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.Tenantbannerurlmsg",'Input field required');
        }else if(!$A.util.isEmpty(Tenantbannerurlval))
        {   
            if(!Tenantbannerurlval.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.Tenantbannerurlmsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.Tenantbannerurlmsg", ''); 
            }
        }else{
            component.set("v.Tenantbannerurlmsg",'');
        }
        if(Tenantnewsmsgval == undefined || Tenantnewsmsgval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantnewsmsgMsg",'Input field required');
        }else{
            component.set("v.TenantnewsmsgMsg",'');
        }if(TenantNewsmsgURLVal == undefined || TenantNewsmsgURLVal == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantNewsmsgURLMsg",'Input field required');
        }else if(!$A.util.isEmpty(TenantNewsmsgURLVal))
        {   
            if(!TenantNewsmsgURLVal.match(website))
            {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.TenantNewsmsgURLMsg", 'Please Enter a Valid URL'); 
            }
            else
            {
                component.set("v.TenantNewsmsgURLMsg", ''); 
            }
        }else{
            component.set("v.TenantNewsmsgURLMsg",'');
        }
        
        
        if(TenantPConnameval == undefined || TenantPConnameval == ""){
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.TenantPConnamemsg",'Input field required');
        }else{
            component.set("v.TenantPConnamemsg",'');
        }
        if(Tenantnameval!=undefined && Tenantsitenameval!=undefined){
        var action = component.get("c.CheckTenantName");
        action.setParams({ "TenantName": Tenantnameval,
                          "Tenantsiteval": Tenantsitenameval});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var bank = response.getReturnValue(); 
            if(bank != '' && bank != undefined) {
                if(bank.CheckTenantUserName == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.tenantnamemsg", 'This Tenant Name already Exist. Please enter a new Tenant Name.');
                } 
                if(bank.CheckTenantsiteName == 'Already Used') { 
                    errorFlag = 'true';
                    component.set("v.validation", true); 
                    component.set("v.tenantsitenamemsg", 'This Tenant Site Name already Exist. Please enter a new Tenant Site Name.');
                }
            } else { 
               // component.set("v.tenantnamemsg", '');
                //component.set("v.tenantsitenamemsg", '');
            }
            if(errorFlag == 'false') {  
            var action = component.get('c.SaveTenant');  
        action.setParams({
            "LoggedUserID" : localStorage.getItem("UserSession"),
            "OrgID": orgSelect,
            "TenantDetails" : component.get("v.Tenant")            
        });
        debugger;
        action.setCallback(this, function(actionResult) { 
            var state = actionResult.getState(); 
            var result = actionResult.getReturnValue();
            if(state === "SUCCESS"){
                var site = $A.get("{!$Label.c.Org_URL}");
                window.location = '/'+site+'/s/administration';
            }
        });
        debugger;
        $A.enqueueAction(action);
        }
            });
        $A.enqueueAction(action);
        }
    },
        onTenantTypeSelectChange: function(component, event, helper) {
        var selectCmp = component.find("Tenanttyppeval").get("v.value");
        if(selectCmp =='Bank')
        {
            component.set("v.EnableBanktype",true); 
        }else{
            component.set("v.EnableBanktype",false);
        }
    },
    
})