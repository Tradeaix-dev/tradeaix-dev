({
    createObjectData: function(component, event) {
        debugger;
        // get the contactList from component and add(push) New Object to List  
        var RowItemList = component.get("v.contactList");
        var opts = [];
				 opts = [
                        { label: "--", value: "--", selected: "true" },
                       /* { label: "Comment Role", value: "Comment Role"},
                        { label: "Validator Role", value: "Validator Role"},
                 	    { label: "Approver Role", value: "Approver Role"}*/]
        RowItemList.push(opts);
        // set the updated list to attribute (contactList) again    
        component.set("v.contactList", RowItemList);
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allContactRows = component.get("v.contactList");
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar++) {
            if (allContactRows[indexVar].FirstName == '') {
                isValid = false;
                alert('First Name Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    },
})