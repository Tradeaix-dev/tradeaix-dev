({
	checkSpecialCharecter : function(data){ 
        var iChars = "!`@#$%^&*()+=-[]\\\';.,/{}|\":<>?~_";   
        for (var i = 0; i < data.length; i++)
        {      
            if (iChars.indexOf(data.charAt(i)) != -1)
            {    
                return true;
            } 
        }
        return false;
    },
})