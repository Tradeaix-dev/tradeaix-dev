({ handleChange: function (component, event, helper) {
    var selectedFields = event.getParam("value");
    var targetName = event.getSource().get("v.name");
    if(targetName == 'srcFields'){ 
        component.set("v.template.SelectedFields", selectedFields);
    }
},
  createTemplate: function(component, event, helper) { 
      
      var action = component.get('c.getCloneProductTemplate');
      var getUrlParameter = function getUrlParameter(sParam) {
          try{
              var searchString = document.URL.split('?')[1];
              var sPageURL = decodeURIComponent(searchString),
                  sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                  sParameterName,
                  i;
              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');                 
                  if (sParameterName[0] === sParam)  {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          }catch(ex){}
      };
      
      var templateId = getUrlParameter('templateId');
      action.setParams({
          "templateId" : templateId
      });
      action.setCallback(this, function(actionResult) {
          debugger;
          var state = actionResult.getState(); 
          if(state === "SUCCESS"){  
              var result = actionResult.getReturnValue();
              component.set("v.template", result);
              debugger;
              if(result.ProductTemplateVExtended.ProductType=='Funded')
              {
                  var pTyperbtnFunded = component.find("rbtnFunded").set("v.value",true); 
                  
              }else{
                  var pTyperbtnUnFunded = component.find("rbtnUnFunded").set("v.value",true); 
              }
              if(result.ProductTemplateVExtended.IsPrimary)
              {
                  var mTyperbtnPrimary = component.find("rbtnPrimary").set("v.value",true); 
              }else{
                  var mTyperbtnSecondary = component.find("rbtnSecondary").set("v.value",true); 
              }
              var approveLevel = component.find("dropApproveLevel");
              approveLevel.set("v.value", result.ProductTemplateVExtended.ApproveLevel);
          } 
          
          else if(state === "ERROR") {
              component.set("v.template", component.get("v.template"));
          }
      }); 
      $A.enqueueAction(action);      
  },
  cancel: function(component, event, helper) {
      component.set("v.validation", false);
      component.set("v.nameError", '');
      
      var getUrlParameter = function getUrlParameter(sParam) {
          try{
              var searchString = document.URL.split('?')[1];
              var sPageURL = decodeURIComponent(searchString),
                  sURLVariables = (sPageURL.replace('&amp;','&')).replace('&amp;','&').split('&'),
                  sParameterName,
                  i;
              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');                 
                  if (sParameterName[0] === sParam)  {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          }catch(ex){}
      };
      
      var templateId = getUrlParameter('templateId');
      window.location = '/demo/s/product-template-detail?templateId='+ templateId;
  },
  
  
  saveTemplate: function(component, event, helper) { 
        debugger;
        var errorFlag = 'false';
        component.set("v.validation", false);  
        var template = component.get("v.template");
        var tempname = component.find("txtTemplateName").get("v.value");
        var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
        var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
            
        if(mTyperbtnPrimary){
            template.IsPrimary=true;
            var market='Primary';
		}
        else if(mTyperbtnSecondary){
            template.IsPrimary=false;
            var market='Secondary';
		}
		
		var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
        var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
            
        if(pTyperbtnFunded){
            template.ProductType='UnFunded';
            var loanType='Funded';
		}
        else if(pTyperbtnUnFunded){
             template.ProductType='UnFunded';
            var loanType='UnFunded';
		} 
         if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.loanError", 'Input field required'); 
        } else { 
            component.set("v.loanError", ''); 
        }
        if(market == '' || market == null || market == undefined || market.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.MytypeError", 'Input field required'); 
        } else { 
            component.set("v.MytypeError", ''); 
        }
        if(tempname == '' || tempname == null || tempname == undefined || tempname.trim().length*1 == 0) {
            errorFlag = 'true';
            component.set("v.validation", true);
            component.set("v.nameError", 'Input field required'); 
        } else { 
            if(helper.checkSpecialCharecter(tempname)) {
                errorFlag = 'true';
                component.set("v.validation", true);
                component.set("v.nameError", 'Please enter without special characters.'); 
            } else {  
                component.set("v.nameError", ''); 
            } 
        }
        var applevel = component.find("dropApproveLevel").get("v.value");
        if(applevel!='-- None --'){
            template.ApproveLevel=applevel;
        }else{
             template.ApproveLevel='';
        }
        if(errorFlag == 'false') {
            component.set("v.CloneProductTEmp", false); 
            template.TemplateName=tempname;
            debugger;
            component.set("v.showProgress",true);
            var img = component.find("pdfImgloading");
            $A.util.removeClass(img,'slds-hide');
            $A.util.addClass(img,'slds-show');  

            component.find("btnSaveTemplate").set("v.disabled", true);
            var action = component.get('c.saveProductTemplate');
            action.setParams({ "template": JSON.stringify(template) });
            action.setCallback(this, function(actionResult) {
                var state = actionResult.getState(); 
                var result = actionResult.getReturnValue();
                if(state === "SUCCESS"){  
                    
                    if(result != 'Already Used' && result != '')
                    {
                        /*var action1 = component.get('c.emailProductTemplate'); 
                        action1.setParams({
                            "templateId" : result  
                        });
                        action1.setCallback(this, function(actionResult1) {
                            var state1 = actionResult1.getState();
                        });
                        $A.enqueueAction(action1); 
                        */
                        
                        component.find("btnSaveTemplate").set("v.disabled", false); 
                        //component.find("btnCancleTemplate").set("v.disabled", false); 
                        
                        component.set("v.validation", false);
                        window.location = '/demo/s/product-template-detail?templateId='+ result;
                                 
                        $A.get('e.force:refreshView').fire();
                    }
                    else if(result == 'Already Used')
                    {
                        component.set("v.validation", true);
                        component.set("v.nameError", 'This Template Name has already been used. Please enter a new Template Name.');
                        component.find("btnSaveTemplate").set("v.disabled", false);
                           
                    } 
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    component.set("v.showProgress",false);
                }
                else if(state === "ERROR") {
                    component.find("btnSaveTemplate").set("v.disabled", false);
                    component.set("v.template", component.get("v.template"));
                    component.set("v.validation", false);
                    component.set("v.showCreate", false); 
                    
                    $A.util.addClass(img,'slds-hide'); 
                    $A.util.removeClass(img,'slds-show');
                    
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams(
                        {
                            'title': 'Failure : ',
                            'message': 'Unable to Created Template',
                            'type': 'Success'
                        }
                    );
                    showToast.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
  
  CloneProductTEmp : function(component, event, helper) { 
      debugger;
      var errorFlag = 'false';
      component.set("v.validation", false);
      var txtTemplateName = component.find("txtTemplateName").get("v.value");
      var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
      var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
      
      if(mTyperbtnPrimary){
          var market='Primary';
      }
      else if(mTyperbtnSecondary){
          var market='Secondary';
      }
      
      var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
      var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
      
      if(pTyperbtnFunded){
          var loanType='Funded';
      }
      else if(pTyperbtnUnFunded){
          var loanType='UnFunded';
      } 
      if(loanType == '' || loanType == null || loanType == undefined || loanType.trim().length*1 == 0) {
          errorFlag = 'true';
          component.set("v.validation", true);
          component.set("v.loanError", 'Input field required'); 
      } else { 
          component.set("v.loanError", ''); 
      }
      if(market == '' || market == null || market == undefined || market.trim().length*1 == 0) {
          errorFlag = 'true';
          component.set("v.validation", true);
          component.set("v.MytypeError", 'Input field required'); 
      } else { 
          component.set("v.MytypeError", ''); 
      }
      
      if(txtTemplateName == undefined || txtTemplateName == '')
      {
          errorFlag = 'true';
          component.set("v.validation", true); 
          component.set("v.nameError", 'Input field required'); 
      }
      else
      { 
          if(helper.checkSpecialCharecter(txtTemplateName))
          {
              errorFlag = 'true';
              component.set("v.validation", true); 
              component.set("v.nameError", 'The Product Template name has special characters. These are not allowed.'); 
          } 
          else{  
              component.set("v.nameError", ''); 
          }  
      } 
      
      if(errorFlag == 'false') {
          //jaya added new function for check existance
          var action = component.get("c.CheckProductTemplate");
          action.setParams({ "txtTemplateName": txtTemplateName });
          action.setCallback(this, function(response) {
              var state = response.getState();
              var requestId = response.getReturnValue(); 
              if(requestId != '') {
                  if(requestId == 'Already Used') { 
                      errorFlag = 'true';
                      component.set("v.validation", true); 
                      component.set("v.nameError", 'This Product Template Name has already been used. Please enter a new Product Template Name.');
                  } else {
                      component.set("v.nameError", ''); 
                  }
              } else { 
                  component.set("v.nameError", ''); 
              }
              if(errorFlag == 'false') {
                  component.set("v.CloneProductTEmp", true); 
              }
          });
          $A.enqueueAction(action); 
      }
  },
  cancelCloneProductTEmp : function(component, event) {
      component.set("v.CloneProductTEmp", false); 
  },
  CloneProductTEmpCheck : function(component, event, helper) {
      if(component.find("chkClone").get('v.value')==true)
      {
          component.find("btnSaveProdTemp").set("v.disabled", false);
      }
      else{
          component.find("btnSaveProdTemp").set("v.disabled", true);
      }
  },
  
  handleProductTypeChange: function(component, event, helper) {
      debugger;
      var templateName = component.find("txtTemplateName").get("v.value");
      var productType = event.getSource().get("v.label");
      if(productType != '')
      {
          component.set("v.loanError", '');    
      }
      debugger;
      var templateName = component.find("txtTemplateName").get("v.value");
      var mTyperbtnPrimary = component.find("rbtnPrimary").get("v.value"); 
      var mTyperbtnSecondary = component.find("rbtnSecondary").get("v.value"); 
      
      if(mTyperbtnPrimary){
          var market='Primary';
      }
      else if(mTyperbtnSecondary){
          var market='Secondary';
      }
       var applevel = component.find("dropApproveLevel").get("v.value");
        if(applevel!='-- None --'){
            var approLevel=applevel;
        }else{
             var approLevel='';
        }
      if(mTyperbtnPrimary || mTyperbtnSecondary) {
          var isPrimary = market; 
          var action = component.get('c.getProductTemplate');
          action.setParams({
              "templateId" : "",
              "productType": productType,
              "marketType": isPrimary.toString(),
              "templateName": templateName,
              "approveLevel": approLevel
          });
          action.setCallback(this, function(actionResult) {
              var state = actionResult.getState(); 
              if(state === "SUCCESS"){  
                  debugger;
                  var result = actionResult.getReturnValue();
                  component.set("v.template.ProductTemplateVExtended", result);
              }
          });
          $A.enqueueAction(action);
      }
  },
  
  handleMarkerTypeChange: function(component, event, helper) {
      debugger;
      var market = event.getSource().get("v.label");
      debugger;
      var templateName = component.find("txtTemplateName").get("v.value");
      var pTyperbtnFunded = component.find("rbtnFunded").get("v.value"); 
      var pTyperbtnUnFunded = component.find("rbtnUnFunded").get("v.value"); 
      if(market != '')
      {
          component.set("v.MytypeError", '');    
      }
      if(pTyperbtnFunded){
          var prodType='Funded';
      }
      else if(pTyperbtnUnFunded){
          var prodType='UnFunded';
      }
      var applevel = component.find("dropApproveLevel").get("v.value");
        if(applevel!='-- None --'){
            var approLevel=applevel;
        }else{
             var approLevel='';
        }
      if(pTyperbtnFunded || pTyperbtnUnFunded) {
          var isPrimary = market; 
          var action = component.get('c.getProductTemplate');
          action.setParams({
              "templateId" : "",
              "productType": prodType,
              "marketType": isPrimary.toString(),
              "templateName": templateName,
              "approveLevel": approLevel
          });
          action.setCallback(this, function(actionResult) {
              var state = actionResult.getState(); 
              if(state === "SUCCESS"){  
                  debugger;
                  var result = actionResult.getReturnValue();
                  component.set("v.template.ProductTemplateVExtended", result);
              }
          });
          $A.enqueueAction(action);
      }
  }, 
 })