trigger RequestInformationQuoteTrigger on Request_Information_Quote__c (before insert) {

try{
for (Request_Information_Quote__c AL: Trigger.new) {

TimeZone tz = UserInfo.getTimeZone();
AL.CreatedDatePDF__c = System.Now().format('dd/MM/YYYY HH:mm:ss', tz.getID());

}
}catch(exception ex){
system.debug('=======Exception======='+ex.getMessage());
}
}