trigger LimitedBiddingTrigger  on Limited_Bidding__c (after insert, after update) {
    EmailManager em = new EmailManager();  
    system.debug('====TenantUserTrigger Inside====');
    if(Trigger.isAfter){
    
    try{
    List<OTP_History__c> OTPListt = NEW List<OTP_History__c>();
    for (Limited_Bidding__c tenantuser: Trigger.new) {
    if(Trigger.isInsert)
    {
    List<String> CcAddresses = new List<String>(); 
    String toEmail = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    
    system.debug('======isInsert=====');                   
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
        String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'CPInvitationEmailTemp' LIMIT 1].Id; 
        String assigneetemaplateId = [SELECT Id from EmailTemplate WHERE Name = 'AssigneeEmailTemp' LIMIT 1].Id;  
        system.debug('======LIMITED BIDDING TRIGGER assigneetemaplateId======'+assigneetemaplateId);
        if(tenantuser.Don_t_Sent_mail__c == false && tenantuser.Assignee_Email__c == false){   
            system.debug('======LIMITED BIDDING TRIGGER======');
            em.sendMailWithTemplate(toEmail , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        } 
        if(tenantuser.Assignee_Email__c == true){            
            em.sendMailWithTemplate(toEmail , CcAddresses , assigneetemaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
        }   
    system.debug('======isInsert=====');
    
    }
    if(tenantuser.agree__c){    
    Integer rndnum = Math.round(Math.random()*1000000);
        
        OTP_History__c OH =NEW OTP_History__c();
        if(string.valueOf(rndnum).length() == 4){
            OH.OTP__c ='91'+string.valueOf(rndnum);
        }  else if(string.valueOf(rndnum).length() == 5)  {
             OH.OTP__c ='9'+string.valueOf(rndnum);
        }else{
             OH.OTP__c =string.valueOf(rndnum);
        }   
        OH.isActive__c =true;
        OH.Limited_Bidding__c =tenantuser.Id;
        OH.OTP_Time_Intervals__c= string.valueOf(System.Now().getTime());
        //OH.OTP_Time_Interval__c = Integer.valueOf(System.Label.OTPTimeinterval);
        OH.OTP_Generated_On__c = System.Now();
        OH.OTP_Expired_On__c = System.Now().AddMinutes(Integer.valueOf(System.Label.OTPTimeinterval));

        OTPListt.add(OH);
    }
       
    if(tenantuser.Do_Not_Agree__c){  
    List<String> CcAddresses = new List<String>();
    system.debug('======isInsert=====');                   
    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationdisagreeEmailTemp' LIMIT 1].Id;
    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
    
    
    }
    if(tenantuser.Not_the_Apropriate_User__c){ 
    List<String> CcAddresses = new List<String>(); 
    system.debug('======isInsert=====');                   
    String toEmail1 = [SELECT User_Email__c from User_Management__c WHERE id=:tenantuser.Tenant_User__c].User_Email__c ;
    List<User_Management__c> listUserEmail= [SELECT User_Email__c from User_Management__c WHERE Id =: tenantuser.CreatedBy__c];                 
    for( User_Management__c lstStr: listUserEmail){
    CcAddresses.add(lstStr.User_Email__c) ;
    }
    String temaplateId = [SELECT Id from EmailTemplate WHERE Name = 'OTPNotificationnotuserEmailTemp' LIMIT 1].Id;
    em.sendMailWithTemplate(toEmail1 , CcAddresses , temaplateId , userinfo.getuserid(), string.valueOf(tenantuser.Id));
    
    
    }
    }
    if(OTPListt.Size()>0){
    Insert OTPListt;
    }
    }
    catch(exception ex){
    system.debug('=======Exception======='+ex.getMessage());
    }
    }
    }