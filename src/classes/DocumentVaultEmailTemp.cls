/**
* DocumentVaultEmailTemp
* This class mainly used for sending the email during the Document Vault sharing functionality
*/
public class DocumentVaultEmailTemp {
	public Id fileId {get;set;} 
    public class DocumentDetails { 
        public String logo{get;set;}
        public String Createduser {get;set;}
        public String shareduser {get;set;}
        public String CreatedDate {get;set;}
        public String Thanksmsg{get;set;}
        public String filename {get;set;}
        public Boolean foldersharing {get;set;}
    }
    public DocumentDetails email {get;set;}  
<<<<<<< HEAD
    // This method is used for get the file and sharig user details
=======
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-dev.git
    public DocumentDetails getdocuments(){
         email = New DocumentDetails();         	
         try{ 
             File_Sharing__c FS = [SELECT Id, Name, Tenant_User__r.First_Name__c,Tenant_User__r.Last_Name__c,Tenant_User__r.User_Email__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,CreatedBy__r.User_Email__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,CreatedBy__r.Tenant__r.Tenant_Footer_Message__c,File_Folder_Name__c from File_Sharing__c WHERE Id =: fileId];
             email.shareduser = FS.Tenant_User__r.First_Name__c+' '+FS.Tenant_User__r.Last_Name__c;
             email.Createduser = FS.CreatedBy__r.First_Name__c+' '+FS.CreatedBy__r.Last_Name__c;            
             email.logo = FS.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c;
             email.Thanksmsg = FS.CreatedBy__r.Tenant__r.Tenant_Footer_Message__c ;
             if(FS.File_Folder_Name__c.contains('Invitation to collaborate')){
                 email.filename = FS.File_Folder_Name__c.replace('- Invitation to collaborate','');
                 email.foldersharing = true;
             }else{
                 email.filename = FS.File_Folder_Name__c;
                 email.foldersharing = false;
             }
             
             
         }catch(exception e){
            system.debug('//****// '+e.getMessage());           
        } 
        return email;
    }
}