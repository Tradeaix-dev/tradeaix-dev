<<<<<<< HEAD
/**
 * LightAttributeDetail
 * This class mainly used for Attributes functionality
 **/
public class LightAttributeDetail {
    @AuraEnabled
public static String getloggeduserBrowserTitle() { 
String Btitle = '';

return '';
}
    // This method is used for getting the Attribute details
    @AuraEnabled
    public static LightAttributeDetailV getAttributeDetails(string attributeId){
        LightAttributeDetailV attribute = new LightAttributeDetailV(); 
        try{
            system.debug('##'+attributeId);
            attribute.result = [SELECT Id, Name, ProductType__c,Mandatory__c,Mandatorys__c, Attribute_Type__c, Attribute_Size__c, IsActive__c, IsPrimary__c, IsSecondary__c, CreatedById, LastModifiedById,CreatedByName__r.First_Name__c, LastModifiedByName__r.First_Name__c,CreatedByName__r.Last_Name__c, LastModifiedByName__r.Last_Name__c, CreatedDate, LastModifiedDate,(Select ID, Field,CreatedDate,CreatedBy.Name , OldValue, NewValue from Histories)  FROM Attributes__c WHERE Id =: attributeId];
            // attribute.result.CreatedByName__c = [SELECT Name FROM User_Management__c WHERE Id =: attribute.result.CreatedByName__c].Name;
            //attribute.result.LastModifiedByName__c = [SELECT Name FROM User_Management__c WHERE Id =: attribute.result.LastModifiedByName__c].Name;
            attribute.Alogs =[SELECT id,CreatedDate,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Operation_Log__c FROM Activity_Log__c WHERE RecordID__c=:attributeId];
           system.debug('###'+attribute.Alogs);
        } catch(Exception ex){
            System.debug('LightAttributeDetail - getAttributeDetails() : ' + ex.getMessage());
            
        }
        return attribute;
    }  
    
    // This method is used for before submit the attributes check the attribute name already used or not
    @AuraEnabled
    public static String CheckTemplateAttributes(String attributeName, String oldAttributeName,String Tenant) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM Attributes__c WHERE Name = : attributeName AND Tenant__c=:Tenant]; 
            if(count > 0 && attributeName != oldAttributeName){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('LightAttributeDetail - CheckTemplateAttributes() : ' + ex.getMessage());
            
            response = '';
        }
        return response; 
    } 
    // This method is used for the update the attributes
=======
public class LightAttributeDetail {
    @AuraEnabled
public static String getloggeduserBrowserTitle() { 
String Btitle = '';

return '';
}
    @AuraEnabled
    public static LightAttributeDetailV getAttributeDetails(string attributeId){
        LightAttributeDetailV attribute = new LightAttributeDetailV(); 
        try{
            system.debug('##'+attributeId);
            attribute.result = [SELECT Id, Name, ProductType__c,Mandatory__c,Mandatorys__c, Attribute_Type__c, Attribute_Size__c, IsActive__c, IsPrimary__c, IsSecondary__c, CreatedById, LastModifiedById,CreatedByName__r.First_Name__c, LastModifiedByName__r.First_Name__c,CreatedByName__r.Last_Name__c, LastModifiedByName__r.Last_Name__c, CreatedDate, LastModifiedDate,(Select ID, Field,CreatedDate,CreatedBy.Name , OldValue, NewValue from Histories)  FROM Attributes__c WHERE Id =: attributeId];
            // attribute.result.CreatedByName__c = [SELECT Name FROM User_Management__c WHERE Id =: attribute.result.CreatedByName__c].Name;
            //attribute.result.LastModifiedByName__c = [SELECT Name FROM User_Management__c WHERE Id =: attribute.result.LastModifiedByName__c].Name;
            attribute.Alogs =[SELECT id,CreatedDate,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Operation_Log__c FROM Activity_Log__c WHERE RecordID__c=:attributeId];
           system.debug('###'+attribute.Alogs);
        } catch(Exception ex){
            System.debug('LightAttributeDetail - getAttributeDetails() : ' + ex.getMessage());
            
        }
        return attribute;
    }  
    
    @AuraEnabled
    public static String CheckTemplateAttributes(String attributeName, String oldAttributeName,String Tenant) { 
        String response = '';
        try {  
            Integer count = [SELECT count() FROM Attributes__c WHERE Name = : attributeName AND Tenant__c=:Tenant]; 
            if(count > 0 && attributeName != oldAttributeName){
                response = 'Already Used';
            }
            else { 
                response = '';
            }  
        } catch(Exception ex){
            System.debug('LightAttributeDetail - CheckTemplateAttributes() : ' + ex.getMessage());
            
            response = '';
        }
        return response; 
    } 
    
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-dev.git
    @AuraEnabled
    public static String EditAttribute(String Id, String AttributeSize, String IsActive, String Ismandatory) {
        String response = '';
        
        try { 
            Boolean mand;
            if(Ismandatory == 'Yes'){
                mand = true;
            }else{
                mand = false;
            }
            Attributes__c attributeNew = new Attributes__c(
                Id = Id,
                //Attribute_Size__c = Decimal.valueOf(AttributeSize),
                IsActive__c = Boolean.valueOf(IsActive),
                Edit_Attribute__c = true,
                Mandatorys__c = Ismandatory,
                Mandatory__c = mand
               
            );  
            if(AttributeSize != '')
               attributeNew.Attribute_Size__c = Decimal.valueOf(AttributeSize); 
            
            update attributeNew;
            response = Id; 
            List<Attributes__c> att =[SELECT id,CreatedByName__c,Name FROM Attributes__c WHERE id=:Id LIMIT 1];
            Helper.ActivityLogInsertCallForRecord(att[0].CreatedByName__c,Id,'Attribute Updated',att[0].Name+' updated.',false);
            
        } catch(Exception ex){
            System.debug('LightAttributeDetail - EditAttribute() : ' + ex.getMessage());
            Helper.ActivityLogInsertCall('','Attribute update-Error',ex.getMessage(),true);
            response = '';
        }
        return response; 
    }
}