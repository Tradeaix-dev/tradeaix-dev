/**
 * UserActiveNotifEmailTempToAdminCtrl
 * This class is mainly used for sending the email notification to admin during tenant user activation
 **/
public class UserActiveNotifEmailTempToAdminCtrl{
    public Id userId {get;set;} 
    public class userDetails {
        public String firstname{get;set;}
        public String Last_Name{get;set;}
        public String User_Email{get;set;}
        public String Salutation {get;set;}
        public String Department{get;set;}
        public String Phone{get;set;}
        public String Mobile{get;set;}
        public String Title{get;set;}
        public String Street{get;set;}
        public String City{get;set;}
        public String State_Province{get;set;}
        public String Zip_Postal_Code{get;set;}
        public String Country{get;set;}

        public string username {get;set;}
        public String logo{get;set;}
        public String urlname{get;set;}
        public String tenantname {get;set;}
        public String userid {get;set;}
        
        }
        public userDetails email {get;set;}  
        public userDetails getUsers()
        {
        email = New userDetails();
        User_Management__c UM = [SELECT Id,User_Title__c,Department__c,Phone__c,Mobile__c,Title__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c, Name, First_Name__c, Last_Name__c, User_Name__c, User_Email__c, Tenant__r.Tenant_Site_Name__c from User_Management__c WHERE ID =: userId];
        email.firstname = UM.First_Name__c;
        email.username = UM.User_Name__c;
        email.logo =system.label.TradeAix_Logo;
        email.tenantname = UM.Tenant__r.Tenant_Site_Name__c;
        email.userid = UM.Id;
        email.Last_Name = UM.Last_Name__c;
        email.User_Email = UM.User_Email__c;
        email.Salutation = UM.User_Title__c;
        email.Department= UM.Department__c;
        email.Phone= UM.Phone__c;
        email.Mobile= UM.Mobile__c;
        email.Title= UM.Title__c;
        email.Street= UM.Street__c;
        email.City= UM.City__c;
        email.State_Province= UM.State_Province__c;
        email.Zip_Postal_Code= UM.Zip_Postal_Code__c;
        email.Country= UM.Country__c;
        try{
        }catch(exception e){
            system.debug('//****// '+e.getMessage());
            email.Country= e.getMessage();
        }
        return email;
    }
}