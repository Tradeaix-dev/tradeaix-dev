public without sharing class LightCorporateDetails {
    @AuraEnabled
    public static LightBankDetailV getBankDetails(string bankId,String LoggeduserTenantID){
        LightBankDetailV bank = new LightBankDetailV(); 
        try{
            if(bankId != null && bankId != ''){
                bank.result = [SELECT CreatedBy__c,Id,Organization__c,IsActive__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,LastModifiedBy__c,Name,Primary_Contact_Email__c,Primary_Contact_Mobile__c,Primary_Contact_Name__c,SystemModstamp,Tenant_Bank_Type__c,Tenant_Banner_Url__c,Tenant_Footer_Message__c,Tenant_Logo_Url__c,Tenant_News_Message_URL__c,Tenant_News_Message__c,Tenant_Short_Name__c,Tenant_Site_Name__c,Tenant_Swift_Code__c,Tenant_Type__c,Tenant_User_Counts__c,Tenant_vision__c,Tenant_Website__c FROM Tenant__c WHERE Id =: bankId];
                bank.TM  =[SELECT id,Tenant_Approved__r.Tenant_Site_Name__c,Tenant_Approved__r.Tenant_Logo_Url__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:bankId AND Selectchk__c=true];     
                bank.Activity_Log=[SELECT id,Action_By__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,CreatedDate,Operation__c,Operation_Log__c,RecordID__c FROM Activity_Log__c WHERE RecordID__c=:bankId AND ErrorCall__c=false ORDER BY CreatedDate DESC LIMIT 100];
                if(LoggeduserTenantID ==System.label.TradeAix_Tenant_Id)
                {
                    bank.TList =[SELECT Tenant__r.Id, Tenant__r.Name,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Logo_Url__c, Tenant__r.Primary_Contact_Email__c,Tenant__r.Tenant_Type__c, Tenant__r.Primary_Contact_Mobile__c, Tenant__r.Primary_Contact_Name__c, Tenant__r.Tenant_Short_Name__c, Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Swift_Code__c, Tenant__r.Tenant_Website__c, Tenant__r.Tenant_User_Counts__c, Tenant__r.IsActive__c, Tenant__r.CreatedDate, Tenant__r.LastModifiedDate,Tenant__r.Tenant_Bank_Type__c,Tenant__r.Tenant_vision__c FROM Tenant_mapping__C where Tenant_Approved__c=:bankId AND Tenant__r.Name!=''];
                }else{
                    bank.TList =[SELECT Tenant__r.Id, Tenant__r.Name,Tenant__r.Organization__r.Name,Tenant__r.Tenant_Logo_Url__c, Tenant__r.Primary_Contact_Email__c,Tenant__r.Tenant_Type__c, Tenant__r.Primary_Contact_Mobile__c, Tenant__r.Primary_Contact_Name__c, Tenant__r.Tenant_Short_Name__c, Tenant__r.Tenant_Site_Name__c, Tenant__r.Tenant_Swift_Code__c, Tenant__r.Tenant_Website__c, Tenant__r.Tenant_User_Counts__c, Tenant__r.IsActive__c, Tenant__r.CreatedDate, Tenant__r.LastModifiedDate,Tenant__r.Tenant_Bank_Type__c,Tenant__r.Tenant_vision__c FROM Tenant_mapping__C where Tenant__c=:LoggeduserTenantID AND Tenant_Approved__c=:bankId AND Tenant__r.Name!='' AND Selectchk__c=true];
                }
                bank.userResults=[SELECT Id, First_Name__c, Last_Name__c,User_Name__c, User_Email__c, Phone__c, Street__c, City__c, 
                                  Country__c, State_Province__c, Zip_Postal_Code__c, Name,Enable_Email__c, 
                                  Enable_Notification__c, Last_Password_Change_or_Reset__c,Profiles__c,
                                  Profiles__r.Name,Tenant__c ,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,
                                  LastModifiedBy__r.First_Name__c,LastModifiedBy__r.Last_Name__c,Tenant__r.City__c,
                                  Tenant__r.Tenant_Site_Name__c, Tenant__r.Organization__r.Name,CreatedDate,
                                  LastModifiedDate,User_Profile__c,Time_Zone__c,Locale__c,Language__c  
                                  from User_Management__c WHERE Tenant__r.Organization__c =: bank.result.Organization__c];

            }            
        } catch(Exception ex){
            System.debug('LightBankDetail - getBankDetails() : ' + ex.getMessage());
            
            Helper.ActivityLogInsertCall('','LightBankDetail - getBankDetails() : ',ex.getMessage(),true);
        }
        return bank;
    }  
    @AuraEnabled
    public static String getloggeduserBrowserTitle1(String FirstName,String LastName,String JobTitle,String Email,String Phone_No,String bid,String Editcall, String uname) {
      
        return '';
        
    }
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        String Btitle = '';
       
        return '';
    } 
    private static Boolean isPublished(String creditUnionId,String Tid) {
     List<Tenant_Mapping__c> lstApproved = [SELECT Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE Tenant__c=:creditUnionId AND Tenant_Approved__c=:Tid];
            Boolean retValue = false;
             for(Tenant_Mapping__c appr : lstApproved){ 
              if(appr.Selectchk__c){
                retValue=true;
                }
                else{
                retValue = false;
                }
             }
        
         return retValue;
    }
   @AuraEnabled
    public static List<SelectListItem> getApprovedCreditUnions(string creditUnionId){
        List<SelectListItem> retValues = new List<SelectListItem>();
        try{
            List<Tenant__c> TList = [SELECT id,Tenant_Site_Name__c FROM Tenant__c Where Tenant_Bank_Type__c='Counterparty Bank' AND CreatedFromOTP__c=false];
           for(Tenant__c appr : TList){ 
                retValues.add(new SelectListItem(
                    appr.Id,
                    appr.Tenant_Site_Name__c,
                    isPublished(
                        creditUnionId,appr.id
                    )
                    
                ));
           }  
            
            retValues.sort();            
        } catch(Exception ex){
            System.debug('getApprovedCreditUnions: ' + ex.getMessage());
        }
        return retValues;
    }
    @AuraEnabled
    public static string addRemoveCounterParty(string creditUnionId, boolean forAdd,String TId,string LoggedUserId){
        system.debug('##addRemoveCounterParty##'+creditUnionId);
        system.debug('##forAdd##'+forAdd);
        system.debug('##LoggedUserId##'+LoggedUserId);
        String retValue = '';
        try{
            
            if(forAdd)
            {
            Tenant_Mapping__c TM =NEW Tenant_Mapping__c();
            TM.Tenant_Approved__c=creditUnionId;
            TM.Tenant__c = TId;
            TM.Selectchk__c =True;
            Insert TM;
                system.debug('+'+TM.id);
                Tenant_Mapping__c[] TMChk = [SELECT id,Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE id=:TM.id Limit 1];
                Helper.ActivityLogInsertCallForRecord(LoggedUserId,TId,'Selected','CP - '+TMChk[0].Tenant_Approved__r.Tenant_Site_Name__c+'- Selected',false);
                
            }else{
                Tenant_Mapping__c[] TMChk = [SELECT id,Tenant_Approved__r.Id, Tenant_Approved__r.Tenant_Site_Name__c,Selectchk__c FROM Tenant_Mapping__c WHERE id=:creditUnionId Limit 1];
                TMChk[0].Selectchk__c = forAdd;
                Helper.ActivityLogInsertCallForRecord(LoggedUserId,TId,'Un-Selected','CP - '+TMChk[0].Tenant_Approved__r.Tenant_Site_Name__c+'- Un-Selected',false);
                delete TMChk;
            }
            retValue = 'Success';
        } catch(Exception ex){
            System.debug('addRemoveCounterParty: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
   
}