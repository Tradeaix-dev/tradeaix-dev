public class ProductTemplateV {
    @AuraEnabled 
    public String ProductType {get; set;}
    @AuraEnabled 
    public String TemplateName {get; set;}
    @AuraEnabled 
    public Boolean IsPrimary = true; 
    @AuraEnabled 
    public List<TemplateField> SourceFields {get;set;}
    @AuraEnabled 
    public List<String> SelectedFields {get;set;}
    @AuraEnabled 
    public String ApproveLevel {get; set;}
}