/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        system.debug('=======startIrl====='+startUrl);
        system.debug('==UserName=='+username);
        system.debug('==password=='+password);
        return Site.login(username, password, startUrl);
    }
    
   	global SiteLoginController () {}
}