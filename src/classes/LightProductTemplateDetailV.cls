public class LightProductTemplateDetailV {
    @AuraEnabled
    public ProductTemplate__c ProdTemp = new ProductTemplate__c();
    
    @AuraEnabled
    public user ProdTempCreatedUser = new user();
    
    @AuraEnabled
    public user ProdTempLstModifiedUser = new user(); 
    
   
    @AuraEnabled
    public List<Product_Template_Object__c> strList { get;set; }
    @AuraEnabled
    public List <Activity_Log__c> Alogs = new List <Activity_Log__c>();
}