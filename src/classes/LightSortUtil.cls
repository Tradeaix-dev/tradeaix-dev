public class LightSortUtil {
	@AuraEnabled    
    public static EnumSortBy SortBy{get;set;}
    @AuraEnabled
    public static EnumSortDirection SortDirection{get;set;}
}