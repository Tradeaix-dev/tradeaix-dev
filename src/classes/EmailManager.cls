/**
* EmailManager
* This is the main generic class for sending emails across the platform
*/
public class EmailManager {  
    @future
    public static void sendMailWithTemplate1(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            
            List<String> GmailList = New List<String>();
            String[] Groupmail = System.label.GroupEmail.split(',');
            for (String str : Groupmail) {
            GmailList.add(str);
            }
            email.setCcAddresses(GmailList);
            
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
            email.setReplyTo('barclays_tradeaix_notification@h-2g29m3l1sy3c1uz1ia6v81x1rgoaa3ox5oyclt8nb99zzpoj48.1k-98cmeay.cs78.apex.sandbox.salesforce.com');
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
    public void sendMailWithTemplate(String address, String[] CcAddresses, String templateId, String targetObjectId, String whatId){  
        try{
            system.debug('==sendMailWithTemplate=='+address+'::'+templateId+'::'+targetObjectId+'::'+whatId);
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();        
            String[] toAddresses = new String[] {address}; 
            email.setToAddresses(toAddresses); 
            //email.setCcAddresses(CcAddresses);
            email.setBccAddresses(CcAddresses);
            email.setTemplateId(templateId);
            email.setTargetObjectId(targetObjectId);
            email.setWhatId(whatId);
             email.setReplyTo('barclays_tradeaix_notification@h-2g29m3l1sy3c1uz1ia6v81x1rgoaa3ox5oyclt8nb99zzpoj48.1k-98cmeay.cs78.apex.sandbox.salesforce.com');
           
            email.setSaveAsActivity(false);            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { email });            
        } catch(exception ex){
            system.debug('//****// '+ex.getMessage());
        } 
    }
}