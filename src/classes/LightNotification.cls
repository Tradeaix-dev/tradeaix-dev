/**
 * LightNotification
 * This class mainly used for Notifications list view
 **/
public with sharing class LightNotification {
    @AuraEnabled
    public static String getloggeduserBrowserTitle() { 
        String Btitle = '';
      
        return '';
    }
    @AuraEnabled
    public static Notificationv1 ShowTable(String Days,String LoggedUserId){ 
        Notificationv1 retValue = new Notificationv1();
        try{
            String query='';
            if(Days == 'Last 7 Days'){
                query = 'SELECT Action__c,Action_By__r.Tenant__r.Tenant_Site_Name__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Action_To__c,CreatedDate,closecall__c,Message__c,Id,ReadFlag__c,Record_Link__c,Reference_Id__c,Name FROM Notification__c WHERE CreatedDate = LAST_N_DAYS:7 AND closecall__c=false AND Action_To__c=\''+LoggedUserId+'\' Order by ReadFlag__c desc,CreatedDate desc';
            }else if(Days == 'Last 30 Days')
            {
                query = 'SELECT Action__c,Action_By__r.Tenant__r.Tenant_Site_Name__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Action_To__c,CreatedDate,closecall__c,Message__c,Id,ReadFlag__c,Record_Link__c,Reference_Id__c,Name FROM Notification__c WHERE CreatedDate = LAST_N_DAYS:30 AND closecall__c=false AND Action_To__c=\''+LoggedUserId+'\' Order by ReadFlag__c desc,CreatedDate desc';
            }else{
                query = 'SELECT Action__c,Action_By__r.Tenant__r.Tenant_Site_Name__c,Action_By__r.First_Name__c,Action_By__r.Last_Name__c,Action_To__c,CreatedDate,closecall__c,Message__c,Id,ReadFlag__c,Record_Link__c,Reference_Id__c,Name FROM Notification__c WHERE closecall__c=false AND Action_To__c=\''+LoggedUserId+'\' Order by ReadFlag__c desc,CreatedDate desc';
            }
            system.debug('query'+query);
            retValue.results = Database.query(query);
            retValue.total_size = Database.countquery('SELECT count() FROM Notification__c WHERE closecall__c=false AND ReadFlag__c=true AND Action_To__c=\''+LoggedUserId+'\'');
            
            
            
        } catch(Exception ex){
            System.debug('Notification - ShowTable() : ' + ex.getMessage());
         
        }
        return retValue;
    }
    @AuraEnabled
    public static string ReadNotification(string Novid){
        
        String retValue = '';
        try{
            Notification__c PCUnion= [SELECT id,ReadFlag__c from Notification__c WHERE id =:Novid];
            PCUnion.ReadFlag__c=false;
            update PCUnion;
        }
        catch(Exception ex){
            System.debug('ReadNotification: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
     @AuraEnabled
    public static string ReadNotification1(string Novid){
        
        String retValue = '';
        try{
            Notification__c PCUnion= [SELECT id,closecall__c from Notification__c WHERE id =:Novid];
            PCUnion.closecall__c=true;
            update PCUnion;
        }
        catch(Exception ex){
            System.debug('ReadNotification: ' + ex.getMessage());
            retValue = 'ex.getMessage()';
        }
        return retValue;
    }
}