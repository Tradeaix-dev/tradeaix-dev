public with sharing class FileReadingClass {
    String fileContent='';
    public PageReference readContent() {
        
        try{
            fileContent = fileBody.toString();
            List<string> allemails = fileContent.split('\n'); 
            Transaction_Attributes__c TAobj = New Transaction_Attributes__c();          
            List<Transaction_Attributes__c> ListTAobj = New List<Transaction_Attributes__c>();
            Integer count = 0;
            Integer prevcount = 0;
            String TFId = ''; 
            Boolean flag = false;
            for(Integer i=0;i<allemails.size();i++) {
                if(allemails[i].contains('MESSAGE HEADER')){
                    prevcount = count;
                    count = count+1;   
                    Transaction__c Trans = new Transaction__c();
                    Trans.Notes__c = 'this is the MT700 sample transactions'+count;
                    Trans.Tenant__c = 'a0f0x000000XTbcAAG';
                    Trans.Product_Template__c = 'a0H0x000003UvVxEAK';
                    Trans.Status__c = 'Transaction Approved';
                    Trans.Loan_Type__c = 'UnFunded';
                    Trans.TransactionRefNumber__c = 'JslJinPURelTMT-10042019'+count;
                    insert Trans;
                    TFId = Trans.Id;
                }
               
                string[] aa = allemails[i].split(':');
                if(aa[0] == '27' ){
                    flag = true;
                }
                if(allemails[i].contains('MESSAGE FOOTER')){
                    flag = false;
                }
                if(prevcount != count && flag == true){   
                    system.debug('====Insert email space==='); 
                    if((!allemails[i].contains('MESSAGE HEADER') ||  !allemails[i].contains('MESSAGE FOOTER')))         
                        if(allemails[i].split(':')[1] != ''){
                            TAobj= new Transaction_Attributes__c(
                            Attribute_Name__c=allemails[i].split(':')[1],                        
                            Attribute_Value__c= allemails[i].split(':')[2],
                            Transaction__c = TFId,
                            Product_Template__c = 'a0H0x000003UvVxEAK');
                            ListTAobj .add(TAobj);
                        }
                                         
                }
                
            }
            
            system.debug('========MessageHeadereCount====='+count);
            insert ListTAobj;
            return null;
                 
        }catch(exception ex){
            system.debug('=====Read Content Exception====='+ex.getMessage());
            system.debug('=====Read Content Exception LINE NUMBER====='+ex.getLineNumber());
        }
       return null;
    }
    public blob fileBody { get; set; }
}