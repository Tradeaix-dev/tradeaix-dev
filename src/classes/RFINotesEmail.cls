/**
* OTPNotificationEmailTemp
* This class mainly used for sending email for adding RFI notes
*/
public class RFINotesEmail {
    public Id notesId {get;set;} 
    public class OfferDetails { 
        public String RequestedName{get;set;}
        public String ReferenceName{get;set;}
        public String CreatedBy {get;set;}
        public String Body {get;set;}
        public String wtq {get;set;}
        public String CreatedDate {get;set;}     
        public String Thanksmsg{get;set;}
        public String logo{get;set;}
        public String logotitle{get;set;} 
    }
    
    public OfferDetails email {get;set;}  
     public OfferDetails getOffers()
    {
        email = New OfferDetails();
        RequestedNote__c RN = [SELECT Id, Name, Body__c, CreatedBy__c, CreatedBy__r.User_Title__c,CreatedBy__r.First_Name__c,CreatedBy__r.Last_Name__c,Requested_To__c,Requested_To__r.User_Title__c,Requested_To__r.First_Name__c,Requested_To__r.Last_Name__c,RFIReference__c,WTQ__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,CreatedBy__r.Tenant__r.Tenant_Footer_Message__c from RequestedNote__c WHERE Id=: notesId ];
        email.RequestedName = RN.Requested_To__r.User_Title__c+' '+RN.Requested_To__r.First_Name__c+' '+RN.Requested_To__r.Last_Name__c;
        email.ReferenceName = RN.RFIReference__c;
        email.CreatedBy = RN.CreatedBy__r.User_Title__c+' '+RN.CreatedBy__r.First_Name__c+' '+RN.CreatedBy__r.Last_Name__c;
        email.Body = RN.Body__c;
        email.wtq = RN.WTQ__c;
        email.logo = RN.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c;
        email.Thanksmsg = RN.CreatedBy__r.Tenant__r.Tenant_Footer_Message__c;
        try{
        }
        catch(exception ex){
            system.debug('exception'+ex.getMessage());
        }
        return email;
        }
}