/**
 * LightningLoginctrl
 * This class is mainly used for login into the application based on the tenant
 **/
global class LightningLoginctrl {
    public  LightningLoginctrl() {
        
    }
    
    /* 
    	Given TenantId it will retrive the tenant details from Tenant__c object    
    */
    @AuraEnabled
    public static Userloginv1 getTenant(String TenantId) {
        Userloginv1 ULogin =NEW Userloginv1();
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
            lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', null);
            system.debug('=====login user===='+userinfo.getName()); 
            ULogin.userlists = [SELECT Id,Tenant_Site_Name__c,Tenant_Short_Name__c,Tenant_Logo_Url__c,Tenant_Banner_Url__c,Tenant_vision__c,Name from Tenant__c WHERE Tenant_Short_Name__c =:TenantId LIMIT 1];
            system.debug('####'+ULogin);
        }
        catch (Exception ex) {
            //return ex.getMessage();            
        }
        return ULogin;
    }
    
    /* 
    	This method used for user login based on the tenant and stored the login history with broswer and IP address  
    */
    @AuraEnabled
    public static String login(String username, String password, String TenantId, string browsername, String ipAddress,String LoginURL,String RetURL) {
        try{
            
            String RetVal ='';
            ApexPages.PageReference lgn;
            //lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', '/demo/s/dashboard');
            if(TenantId =='jindal')
            { 
                system.debug('#RetURL#'+RetURL);
                if(RetURL.contains('undefined')){
                    lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/dashboard');
                    
                }else{
                    lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/transactionsummary?'+RetURL);
                    
                    
                }
            }else if(TenantId =='reliance' || TenantId=='rrl' || TenantId=='rpl'){
                lgn = Site.login('reliancecom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/dashboard');
            }
            else if(TenantId =='kotak'){
                lgn = Site.login('kotakcom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/dashboard');
            }
            else if(TenantId =='admin'){
                lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/dashboard');
            }
            else if(TenantId =='dbs'){
                lgn = Site.login('dbscom_user@gmail.com.tradeaix99', 'trade@1234', System.label.Org_URL+'/s/dashboard');
            }
             else if(TenantId =='ebt'){
                lgn = Site.login('ebtcom_user@gmail.com.tradeaix44', 'trade@1234', System.label.Org_URL+'/s/dashboard');
            }
            else{
                 lgn = Site.login('tradeaixcom_user@gmail.com.tradeaix99', 'trade@123', System.label.Org_URL+'/s/dashboard');
            }
            system.debug('===userinfo=='+userinfo.getUserId());
            RetVal = Helper.getLoginUserDetails(username,password,TenantId,browsername,ipAddress,LoginURL);
            if(RetVal =='Failed'){
               return RetVal;
            }else{                    
                aura.redirect(lgn);
           }
            
            Helper.ActivityLogInsertCall(RetVal,'Login',username+'- successfully logged.',false);
            return RetVal;
            
        }
        catch (Exception ex) {
            Helper.ActivityLogInsertCall('','Login Error',ex.getMessage(),true);
            return ex.getMessage();            
        }
    }
    
    // Below methods are default site login method
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }
    
    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }
    
    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }
    
    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}