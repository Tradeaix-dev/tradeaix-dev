/**
* LightManualReports
* This class mainly used for Reports
*/
public without sharing class LightManualReports {
    @AuraEnabled
    public static LightManualReportsV getreports(){
        LightManualReportsV  listReports = new LightManualReportsV ();
        listReports.reportlists = [SELECT Id, Name,URL__c,CreatedBy__c,CreatedBy__r.First_Name__c,CreatedDate,CreatedBy__r.Last_Name__c,LastModifiedBy__r.First_Name__c, LastModifiedBy__r.Last_Name__c, Description__c,LastModifiedBy__c,Name__c FROM Report__c order by List_Order_c__c];
        return listReports;
    }
      @AuraEnabled
    public static LightManualReportsV getAttributes(){
        LightManualReportsV  listReports = new LightManualReportsV ();
        listReports.AttributesList = [SELECT Id, Name,Tenant__c,Tenant_Name__c,CreatedByName__c,CreatedByName__r.First_Name__c,CreatedDate,CreatedByName__r.Last_Name__c,LastModifiedByName__r.First_Name__c,IsPrimary__c,LastModifiedByName__r.Last_Name__c,UnFunded__c,Funded__c,LastModifiedByName__c,IsActive__c,Attribute_Type__c,Mandatorys__c FROM Attributes__c];
        return listReports;
    }
    @AuraEnabled
    public static LightManualReportsV getTemplates(){
        LightManualReportsV  listReports = new LightManualReportsV ();
        listReports.getTemplates = [SELECT Id,TemplateName__c,CreatedDate,Tenant_Name__c,ProductType__c,IsPrimary__c,Active__c,CreatedBy_Name__c,LastModifiedBy_Name__c FROM ProductTemplate__c WHERE Created_ByName__c!=NULL];
        return listReports;
    }
      @AuraEnabled
    public static LightManualReportsV getcp(){
        LightManualReportsV  listReports = new LightManualReportsV ();
        listReports.getcp = [SELECT Id,Tenant_Site_Name__c,Tenant_Swift_Code__c,IsActive__c,Street__c,City__c,State_Province__c,Zip_Postal_Code__c,Country__c,Primary_Contact_Name__c,Primary_Contact_Email__c,Primary_Contact_Mobile__c,CreatedBy_Name__c,CreatedDate From Tenant__c WHERE Tenant_Bank_Type__c='Counterparty Bank'];
        return listReports;
    }
    @AuraEnabled
    public static LightManualReportsV getQuotes(){
        LightManualReportsV  listReports = new LightManualReportsV ();
        listReports.getQuotes = [SELECT Id,Transaction__r.Currency__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Status__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,Transaction__r.Loan_Type__c,Bid_Amount__c,Bid_Service_Fee_bps__c,Bid_Expiration_Date__c,Bid_Status__c,FTenant_Name__c,CreatedDate FROM Quotes__c];
        return listReports;
    }
}