public class LightBankListV { 
    @AuraEnabled
    public List <Tenant__c> results = new List <Tenant__c>();  
     @AuraEnabled
    public List <Tenant_Mapping__c> TMresults = new List <Tenant_Mapping__c>();
    @AuraEnabled
    public Integer counter = 0; 
    @AuraEnabled
    public String selectedItem = ''; 
    @AuraEnabled
    public String showpage = '';
    @AuraEnabled
    public String sortbyField = '';
    @AuraEnabled
    public String sortDirection = '';
    @AuraEnabled
    public Integer total_page = 0; 
    @AuraEnabled
    public Integer list_size = 0;
    @AuraEnabled
    public Integer total_size = 0; 
    @AuraEnabled
    public String Adminchk = '';
    @AuraEnabled
    public String tranRefnumber = '';
    @AuraEnabled
    public List<String> countrycodelist {get;set;}
    @AuraEnabled
    public List<String> countrylist {get;set;}
    @AuraEnabled
    public List <User_Management__c> UMresults = new List <User_Management__c>();
    @AuraEnabled
    public List <Quotes__c> quoteresults = new List <Quotes__c>();
     
    
}