/**
 * BidClosedEmailTemp
 * This class is mainly used for sending Quote closure email
 **/
public without sharing class BidClosedEmailTemp {
    public Id quoteId {get;set;} 
    public class QuoteDetails { 
        public String CreatedBy {get;set;}
        public String firstname {get;set;}
        public String partyName {get;set;}
        public String logourl {get;set;}
        public Boolean logourlhasvalue {get;set;}
        public String Thanksmsg{get;set;}
    }
    public QuoteDetails email {get;set;}  
<<<<<<< HEAD
     // Get the Quote details for Email Template
=======
>>>>>>> branch 'master' of https://Tradeaix-dev@bitbucket.org/Tradeaix-dev/tradeaix-dev.git
    public QuoteDetails getQuotes(){
        email = New QuoteDetails();
        Quotes__c quoteRef = [SELECT Id,Name,Bid_Amount__c,Bid_Expiration_Date__c,Bid_Service_Fee_bps__c,Bid_Status__c,Note__c,Reason__c,CreatedBy__r.First_Name__c, CreatedBy__r.Last_Name__c,CreatedBy__r.Tenant__r.Tenant_Site_Name__c,CreatedBy__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.TransactionRefNumber__c,Transaction__r.Currency__c,Transaction__r.Tenant__r.Tenant_Site_Name__c,Transaction__r.Tenant__r.Tenant_Logo_Url__c,Transaction__r.Issuing_Banks__r.Tenant_Site_Name__c,CreatedBy__r.Profiles__c,Transaction__r.CreatedBy__r.First_Name__c,Transaction__r.Tenant__r.Tenant_Footer_Message__c,CreatedBy__r.User_Title__c from Quotes__c WHERE Id =: quoteId];
        if(quoteRef.CreatedBy__r.User_Title__c != null){
                 email.firstname = quoteRef.CreatedBy__r.User_Title__c+' '+quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }else{
                 email.firstname = quoteRef.CreatedBy__r.First_Name__c+' '+quoteRef.CreatedBy__r.Last_Name__c;
             }  
        if(quoteRef.CreatedBy__r.Tenant__r.Tenant_Logo_Url__c == null){            
            email.logourl = system.label.TradeAix_Logo;
        }else{
            email.logourl = quoteRef.Transaction__r.Tenant__r.Tenant_Logo_Url__c;
        }
        email.Thanksmsg = quoteRef.Transaction__r.Tenant__r.Tenant_Footer_Message__c;
        email.partyName = quoteRef.Transaction__r.Tenant__r.Tenant_Site_Name__c;
        try{
        }catch(exception ex){
            email.firstname = ex.getMessage();
            system.debug('===BidSubmitEmailTempException==='+ex.getMessage());
        }
        return email;
    }
}