global with sharing class CommunitiesLoginController {
    
    global CommunitiesLoginController () {}
    
    global PageReference forwardToAuthPage() {
    	String startUrl = System.currentPageReference().getParameters().get('startURL');
    	String displayType = System.currentPageReference().getParameters().get('display');
        return new PageReference('/s/login?startURL=' +EncodingUtil.urlEncode(startURL, 'UTF-8'));
    }
    
}